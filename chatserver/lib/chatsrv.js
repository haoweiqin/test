
// Export a convenience function that creates an instance of chatserver
module.exports = function(io) {
  return new chatserver(io);
}

// The actual constructor function, which is also a closure 
// holding all the private state information

function chatserver(io)
{
  // Reference to "this" that won't get clobbered by some other "this"
  var self = this;

  var dbmfs = require("./dbm.js")();
  dbmfs.connect(function (error) {
    log("mongoose.connected");
    if (error) {
        log("Failed: " + error);
    }
  });

  
  var usermgr = require("./user.js")(dbmfs);
  var cgmgr = require("./chatgrp.js")(dbmfs, usermgr);
  var msgmgr = require("./msg.js")(dbmfs, cgmgr);
  var msgtmgr = require('./msgtemplate.js')(dbmfs);

  // Define log function
  var util = require("../util/util.js");
  var utilnode = require("util");
  function log(args){
    var fmtstr = utilnode.format.apply(this, arguments);
    util.log("[chatsrv] "+ fmtstr);
  }

  self.msgtmgr = msgtmgr;

  // Export usermgr out
  self.usermgr = usermgr;
  log('usermodel %j', usermgr.usermodel);

  // Store active clients - uid <-> socket
  var clients = {};
  // Reverse table - socket.id <-> uid
  var uids = {};

  // Listen on chat namespace for incoming connection
  chat = io.of('/chat')
  chat.on('connection', function(socket){
    log('a user connected with socket id: %s', socket.id);
    socket.auth = false;

    // store the client info
    socket.on('startchat', function(uid){
      if (uid == null)
        return log('invalid uid');

      log('ready to chat uid: %s', uid);
      clients[uid] = socket;
      uids[socket.id] = uid;
      socket.auth = true;
    });

    // When socket disconnect
    socket.on('disconnect', function(){
      log('a user disconnected %s', socket.id);
      if (!(socket.id in uids)) {
        return log('warning! socket id {%s}  not found in uids', socket.id);
      }

      // Remove uid and client
      var uid = uids[socket.id];
      log('Remove %s from clients', uid);
      delete uids[socket.id];
      delete clients[uid];
    });

    // -------------------- user    opeartion --------------
    socket.on('createuser', usermgr.createUser);
    socket.on('updateavatar', usermgr.updateAvatar);

    // -------------------- chatgroup opeartion --------------
    // Handle 'createChatgroup'
    //   user.js handle user related operations: adduser, modifyuser, change groups, etc...
    // TODO: 
    //  create chatgroup,
    //  add chatgroup to involved users(2 users now)
    socket.on('createchatgroup', cgmgr.createChatgroup);

    socket.on('ensurechatgroup', cgmgr.ensureChatgroup);

    socket.on('ensurechatgroupIOS', function(userids, callback){
      var users=[]
      userids.forEach(function(uid){
        users.push({_id:uid});
      });
      cgmgr.ensureChatgroup(users, callback);
    });

    // Handle 'Update'
    // TODO: Handle 'getchatgroup'
    // if unable get a session, then create a new one

    // -------------------- message operation --------------
    // Handle 'sendmessage'
    socket.on('sendmessage', function(msginfo){

      log('get new message from client ', msginfo)
      if (!(socket.id in uids))
        return log('Error: client user {%s} did not register for chat yet, please send startchat msg', msginfo.from);

      msgmgr.saveMessage(msginfo, function(msgid){
        if (msgid == null)
          return log('save new message failed');

        //if ("to" in msginfo) {
          var touid = msginfo.to;
          if (touid == null)
            return;

          log('msg to send to %s', touid);
          if (touid in clients) {
            var msgdata = {
              msg: msginfo.msg,
              msgid: msgid, // is msgid necessary?
              from: msginfo.from
            };
            clients[touid].emit("newmsg", msgdata, function(res){
              log('ACK from client, msg sent to %s with msgid %s', touid, msgid);
              msgmgr.updateMessage(msgid);
            });
          }
        //}
      });
    });

    // handle 'getmessage'
    socket.on('getmessage', function(uid){
      msgmgr.getMessage(uid, function(err, results){

        // function - done (when got all chatgroups messages)
        log("msg map completed for user ", uid);

        if (err) return log("get message error {%s} for user %s", err, uid);

        //if (results == null || results[0] == null)
        //  return log("No chatgroup or msg found for user "+uid);

        log('user {%s} has %d chatgroups', uid, results.length);
        var validmsgdata = [];
        results.forEach(function(msgdata) {
          if(msgdata != null) {
            validmsgdata.push(msgdata);
            log('chatgroup {%s} has %d messages', msgdata.cg._id, msgdata.msgs.length);
          }
        });

        log("totally %d chatgroup found that has messages", validmsgdata.length)
        if (validmsgdata.length == 0)
          return log('No valid chatgroup found for user %s', uid);

        socket.emit("msgdata", validmsgdata, function(res){
          log("Response from client {%s}, socket id {%s}", res, socket.id);
          // TODO: mark messages that has been received by client
        });
      });
    });

    // get msg template
    socket.on('getmsgtemplate', function(langid, cb){
      msgtmgr.getMsgTemplate(langid, cb);
    });

  });
}

