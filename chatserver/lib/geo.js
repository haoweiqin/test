var MaxDistanceDegreeMin = 0.00001; // ~ 1 meter
var MaxDistanceDegreeMax = 45; // ~5,000 kilometer

var RadianToDegree = 180 / Math.PI;
var DegreeToMeter = 1000000 / 9; // 111,111.1111 meter per degree

module.exports.updateSchemaParam = function(schemaParam) {
	//dev:log("geo.updateSchemaParam " + jsnk(schemaParam));
	schemaParam.hasLoc = Boolean;
	schemaParam.location = {
		type: {
			type: String,
			default: "Point"
		},
		coordinates: [Number]
	};
}

module.exports.updateIndexParam = function(indexParam) {
	//dev:log("geo.updateIndexParam " + jsn(indexParam));
	indexParam.location = "2dsphere";
}

module.exports.preSave = function(row) {
	var lon;
	var lat;
	if (row.hasLoc) {
		if (row.location
			&& "Point" == row.location.type
			&& row.location.coordinates
			&& row.location.coordinates.length == 2) {
			if ((lon = row.location.coordinates[0]) >= -180
				&& lon < 180
				&& (lat = row.location.coordinates[1]) >= -90
				&& lat < 90) {
				return;
			}
			row.location.coordinates = normalizeCoord(lon, lat);
			return;
		}
		row.hasLoc = false;
		row.location = { type : "Point", coordinates : randomCoord() };
		return;
	}
	if (row.location
		&& row.location.coordinates
		&& "Point" == row.location.type
		&& row.location.coordinates.length == 2
		&& (lon = row.location.coordinates[0]) >= -180
		&& lon < 180
		&& (lat = row.location.coordinates[1]) >= -90
		&& lat < 90) {
		return;
	}
	row.location = { type : "Point", coordinates : randomCoord() };
	return;
}

function normalizeCoord(lon, lat) {
	//dev:log("geo.normalizeCoord lon=" + lon + " lat=" + lat);
	var lz = lon + 180;
	var lq = lz - Math.floor(lz / 360) * 360 - 180;
	var pz = lat + 90;
	var pq = pz - Math.floor(pz / 180) * 180 - 90;
	return [ lq, pq ];
}

function randomCoord() {
	var lon = Math.random() * 360 - 180;
	var lat = Math.acos(2 * Math.random() - 1) * RadianToDegree - 90;
	//dev:log("geo.randomCoord lon=" + lon + " lat=" + lat);
	return [ lon, lat ];
}

module.exports.updateLocation = function(row, hasLoc, lon, lat) {
	//dev:log("geo.updateLocation row=" + jsn(row) + " hasLoc=" + hasLoc + " lon=" + lon + " lat=" + lat);
	row.hasLoc = hasLoc ? true : false;
	row.location = { type : "Point", coordinates : hasLoc ? normalizeCoord(lon, lat) : randomCoord() };
}

module.exports.search = function(Model, lon, lat, maxDistanceDegree, query, limit, cbErrResults) {
	//dev:log("geo.search lon=" + lon + " lat=" + lat + " maxDegree=" + maxDistanceDegree + " query=" + jsn(query) + " limit=" + limit);
	var maxDistanceMeter = Math.max(MaxDistanceDegreeMin, Math.min(MaxDistanceDegreeMax, maxDistanceDegree)) * DegreeToMeter;
	query.hasLoc = true;
	//dev:log("geo.search maxDistanceMeter=" + maxDistanceMeter + " query=" + jsn(query));
	Model.geoNear({
			type : "Point",
			coordinates : normalizeCoord(lon, lat)
		}, {
			spherical : true,
			maxDistance : maxDistanceMeter,
			num: limit,
			query : query
		}, function(err, results, stats) {
			if (err) {
				return cbErrResults(new Error("geo.search fail at Model.geoNear " + err));
			}
			cbErrResults(err, results);
		});
}

// ### helper
function log(msg) {
	console.log(new Date().toJSON() + " " + msg);
}
function jsn(obj) {
	return JSON.stringify(obj);
}
function jsnk(obj) {
	var keys = "{";
	for (var key in obj) {
		keys += key + ":?,";
	}
	keys += "?}";
	return keys;
}
