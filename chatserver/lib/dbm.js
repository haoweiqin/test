
// Export a convenience function that creates an instance
module.exports = function() {
  return new dgmfs();
}


// The actual constructor function, which is also a closure 
// holding all the private state information

function dgmfs() {
  // Reference to "this" that won't get clobbered by some other "this"
  var self = this;

  // Private state variables
  var mongodbpath="mongodb://127.0.0.1/itisdb";
  // Mongoose variable; require here works as Node.js search "node_modules" of
  // currently-executing js file and then walk its way up the folder hierarchy,
  // checking each level for a node_modules folder
  var mongoose = require('mongoose');
  self.mongoose = mongoose;
  
  // connect to mongodb
  self.connect = function(callback) {
    mongoose.connect(mongodbpath, function (error) {
        callback(error);
    });
  };
}


