// Point of Interest

var util = require('util');
var geo = require('./geo.js');

// Export a convenience function that creates an instance
module.exports = function(dgmfs) {
	return new usermgr(dgmfs);
}

// The actual constructor function, which is also a closure 
// holding all the private state information
function usermgr(dgmfs) {

	// Reference to 'this' that won't get clobbered by some other 'this'
	var self = this;

	// Private state variables
	var schema = dgmfs.mongoose.Schema;

	// Define log function
	var util = require('../util/util.js');
	var utilnode = require('util');
	function log(args){
		var fmtstr = utilnode.format.apply(this, arguments);
		util.log('[poi] '+ fmtstr);
	}

	// Define user schema
	var poischema = new schema({
		_id:String, //itisid
		dispname: String,
		hasLoc: Boolean,
		location: { type:{type:String, default:'Point'}, coordinates: [Number] },
		description: String
	},
	{
		collection: 'poi'
	});

	// define index for location field
	poischema.index({ location: '2dsphere' });

	// export user schema
	self.poischema = poischema;

	// Define user model
	var poimodel = dgmfs.mongoose.model('poi', self.poischema);
	self.poimodel = poimodel;

	// Pre save 'poi'
	poischema.pre('save', function (next) {
		var self = this;
		log('pre save poi {%s}', self._id);
		geo.preSave(self);
		if (!self.isNew) {
			log('Modify existing poi document');
			return next(new Error('Existing'));
    	}

		poimodel.find({ _id : self._id }, function(err, docs) {
			log('find');
			if (!docs.length) {
				return next();
			}
			next(utilnode.format('poi id %s already exist', self._id));
		});
	});

	// Implement create poi 
	function createPoiImpl(uid, displayname, callback){
		var u = new poimodel({
			_id: uid,
			dispname:displayname
		});
		geo.preSave(u);
		u.save(function(err){
			callback(err);
		});
	};

	// Export createpoi function
	self.createpoi = function(udata) {

		// Try to create poi
		createpoiImpl(udata._id, udata.dispname, function(err) {
			if (err)
			return log('create poi %s failed {%s}', udata._id, err);
 
			log('create poi {%s} success.', udata._id);
		});
	};
}
