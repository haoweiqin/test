
// Export a convenience function that creates an instance of message manager
module.exports = function(dgmfs) {
  return new msgtmgr(dgmfs);
}


// The actual constructor function, which is also a closure 
// holding all the private state information

function msgtmgr(dgmfs) {

  // Reference to "this" that won't get clobbered by some other "this"
  var self = this;
  
  // Define log function
  var util = require("../util/util.js");
  var utilnode = require("util");
  function log(args){
    var fmtstr = utilnode.format.apply(this, arguments);
    util.log("[msgtemplate] "+ fmtstr);
  }

  // Require async library
  // var async = require('async');

  // Private state variables
  var schema = dgmfs.mongoose.Schema;
  var mongoose = dgmfs.mongoose;

  // Define message template schema
  var msgtschema = new schema({
    id: Number,
    langid: Number,
    msg: String
    },
    {collection: 'msgtemplate'}
  );

  msgtschema.index({ id: 1, langid: -1 }, { unique: true })

  self.msgtschema = msgtschema;

  // Define message template model
  var msgtmodel = dgmfs.mongoose.model('msgtemplate', msgtschema);
  self.msgtmodel = msgtmodel;

  // Pre save 'msgtemplate'
  msgtschema.pre('save', function (next) {
    var self = this;
    next(null, self);
  });

  // Export getMessage function
  // Get all message template msgs
  self.getMsgTemplate = function(langid, callback){
    log("___get message template for langid {%s}", langid);
    //var query = msgtmodel.find({});
    //query.where("_id", "560f4e0be9fa1ea5d8312f69");
    //query.where("langid", "2");
    msgtmodel.find({langid:langid}, function(err, msgts){
      if (err != null) {
        log(err);
        return callback('failed');
      }
      if(msgts == null || msgts.length == 0) {
        log('cannot find msgts');
        return callback("unable to find msgs templates");
      }

      log("found {%d} message template for langid {%s}", msgts.length, langid);
      callback(null, msgts);
    });
  };
}

