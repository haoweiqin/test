// Export a convenience function that creates an instance of session manager
module.exports = function(app) {
  return new signup(app);
}

// The actual constructor function, which is also a closure
// holding all the private state information
function signup(app) {

  // Reference to 'this' that won't get clobbered by some other 'this'
  var self = this;

  var Fiber = require('fibers');
  var Canvas = require('canvas');
  var bcrypt = require('bcrypt');
  var nodemailer = require('nodemailer');
  var util = require('../util/util.js');
  var utilnode = require('util');
  var mongoose = require('mongoose');
  var ObjectId = mongoose.Types.ObjectId;

  function fib() {
    if (arguments.length == 2) {
      new Fiber(arguments[1]).run(arguments[0]);
      return;
    }
    new Fiber(arguments[0]).run();
  }

  function cbk() {
    var fiber = Fiber.current;
    var names = [].splice.call(arguments, 0);
    return function() {
                var res = {};
                for (var pos = 0; pos < names.length; pos++) {
                        res[names[pos]] = arguments[pos];
                }
                if (!fiber.itis_isYielding) {
                        fiber.itis_syncRes = res;
                        return;
                }
                fiber.run(res);
        }
  }

  function yld() {
        var fiber = Fiber.current;
        var syncRes = fiber.itis_syncRes;
        if (syncRes) {
                fiber.itis_syncRes = null;
                return syncRes;
        }
        fiber.itis_isYielding = true;
        var res = Fiber.yield();
        fiber.itis_isYielding = false;
        return res;
  }

  var Model;
  var senderEmail;
  var senderPassword;
  var noSend;

  //--define app.get related to singup
  app.get('/signup', function(req, res) {
    log('app.get /signup');
    res.render('pages/signup');
  });

  //TODO:user id check
  app.post('/ajax/nameChk', function(req, res) {
    fib(function() {
      log('req.body=%j', req.body);
      var name = dec(req.body.name);
      log('app.post /ajax/nameChk name=' + name);
      var out = yld(self.nameChk(name, cbk('err')));
      return res.send(jsn(out));
    });
  });

  //Q: is the pass encrypted, we may embedded pass check in client
  //A: the final signup method will call passChk again; this is a just pre-checking.
  //   all https is encrypted; and https post body is recommended for transferring sensitve info.
  //   it may be moved to client side javscript.
  app.post('/ajax/passChk', function(req, res) {
    var pass = dec(req.body.pass);
    log('app.post /ajax/passChk pass=' + pass);
    var out = self.passChk(pass);
    return res.send(jsn(out));
  });

  var cookieParser = require('cookie-parser');
  app.use(cookieParser());
  var session = require('express-session');
  app.use(session({
    secret: Math.random().toString(),
    resave: false,
    saveUninitialized: false
  }));

  app.get('/captcha.jpg', function(req, res) {
        fib(function() {
                log('app.get /captch.jpg session=' + jsn(req.session));
                var out = yld(self.captchaImg(req.session, cbk('err', 'img')));
                if (out.err) {
                        return res.status(404).send(out);
                }
                res.type('jpg');
                res.end(out.img);
        });
  });

  app.post('/ajax/captchaRegen', function(req, res) {
        log('app.post /ajax/captchaRegen session=' + jsn(req.session));
        self.captchaRegen(req.session);
        res.send(jsn({}));
  });

  app.post('/ajax/captchaChk', function(req, res) {
        var input = dec(req.body.captcha);
        log('app.post /ajax/captchaChk input=' + input);
        var out = self.captchaChk(req.session, input);
        res.send(jsn(out));
  });

  app.post('/ajax/emailSend', function(req, res) {
        fib(function () {
                var email = dec(req.body.email);
                log('app.post /ajax/emailSend email=' + email);
                var out = yld(self.emailSend(req.session, email, cbk('err')));
                res.send(jsn(out));
        });
  });

  app.post('/ajax/emailCodeChk', function(req, res) {
        var emailCode = dec(req.body.emailCode);
        log('app.post /ajax/emailCodeChk emailCode=' + emailCode);
        var out = self.emailCodeChk(req.session, emailCode);
        res.send(jsn(out));
  });

  app.post('/ajax/signup', function(req, res) {
        fib(function () {
                var name = dec(req.body.name);
                var pass = dec(req.body.pass);
                var captcha = dec(req.body.captcha);
                var emailCode = dec(req.body.emailCode);
                log('app.post /ajax/signup name=' + name + ' captcha=' + captcha + ' emailCode=' + emailCode);
                var out = yld(self.signup(req.session, name, pass, captcha, emailCode, cbk('err', 'loginUser')));
                if (out.err) {
                        return res.send(jsn(out));
                }
                req.session.loginUser = out.loginUser;
                out.loginUser = null;
                res.send(jsn(out));
        });
  });

  app.get('/welcome', function(req, res) {
        log('app.get /welcome session=' + req.session);
        var loginUser = req.session.loginUser;
        if (!loginUser) {
        	return res.render('pages/nosignin');
        }
        res.render('pages/welcome', { loginUser: loginUser });
  });

  app.get('/signin', function(req, res) {
        log('app.get /signin');
        res.render('pages/signin');
  });

  app.get('/signout', function(req, res) {
        log('app.get /signout session=' + req.session);
        req.session.destroy();
        res.send('<h1>You are logged out</h1><br/><a href=/signin>Sign In Again</a><br/><a href=/signup>Sign Up New User</a>');
  });

  app.post('/ajax/signin', function(req, res) {
        fib(function () {
                var name = dec(req.body.name);
                var pass = dec(req.body.pass);
                log('app.post /ajax/signin name=' + name);
                var out = yld(self.signin(name, pass, cbk('err', 'loginUser')));
                if (out.err) {
                        return res.send(jsn(out));
                }
                req.session.loginUser = out.loginUser;
                out.loginUser = null;
                res.send(jsn(out));
        });
  });

	app.get('/admin/users', function(req, res) {
		fib(function() {
			log('app.get /admin/users');
			var loginUser = req.session.loginUser;
			if (!loginUser || !loginUser.isAdmin) {
				return res.render('pages/error', { message: 'Access denied' });
			}
			var page = req.query.page ? parseInt(req.query.page) : req.session.sessPage ? req.session.sessPage : 0;
			req.session.sessPage = page;
			var out = yld(self.users(page, cbk('err', 'users')));
			if (out.err) {
				return res.render('pages/error', { message: jsn(out.err) });
			}
			res.render('pages/users', {
				page: page,
				users: out.users
			});
		});
	});

	app.get('/marker', function(req, res) {
		log('app.get /marker');
		var lng = parseFloat(req.query.lng);
		var lat = parseFloat(req.query.lat);
		var txt = req.query.txt;
		res.render('pages/marker', { lng: lng, lat: lat, txt: txt });
	});

	app.get('/admin/edit', function(req, res) {
		fib(function() {
			log('app.get /admin/edit');
			var loginUser = req.session.loginUser;
			if (!loginUser || !loginUser.isAdmin) {
				return res.render('pages/error', { message: 'Access denied' });
			}
			var id = req.query.id;
			var condition = { _id : id };
			if (!id || !condition) {
				return res.render('pages/error', { message: 'No condition' });
			}
			var out = yld(Model.find(condition, cbk('err', 'rows')));
			log(jsn(condition) + ' -> ' + jsn(out));
			if (out.err || out.rows.length < 1) {
				return res.render('pages/error', { message: jsn(out.err) });
			}
			res.render('pages/edit', {
				condition: condition,
				user: out.rows[0]
			});
		});
	});

	app.post('/ajax/admin/update', function(req, res) {
		fib(function() {
			log('app.get /ajax/admin/update');
			var loginUser = req.session.loginUser;
			if (!loginUser || !loginUser.isAdmin) {
				return res.send(jsn({ err: 'Access denied' }));
			}
			var condition = JSON.parse(dec(req.body.condition));
			var user = JSON.parse(dec(req.body.user));
			if (!condition || !user) {
				return res.send(jsn({ err: 'Missing parameters' }));
			}
			for (var key in condition) {
				delete user[key];
			}
			if (user._id) {
				delete user['_id'];
			}
			var out = yld(Model.update(condition, user, cbk('err', 'raw')));
			log(jsn(condition) + ' ' + jsn(user) + ' -> ' + jsn(out));
			if (out.err) {
				return res.send(jsn({ err: jsn(out.err) }));
			}
			res.send(jsn({}));
		});
	});

	app.post('/ajax/admin/insert', function(req, res) {
		fib(function() {
			log('app.get /ajax/admin/insert');
			var loginUser = req.session.loginUser;
			if (!loginUser || !loginUser.isAdmin) {
				return res.send(jsn({ err: 'Access denied' }));
			}
			var user = JSON.parse(dec(req.body.user));
			if (!user) {
				return res.send(jsn({ err: 'Missing parameters' }));
			}
			var out = yld(new Model(user).save(cbk('err')));
			log(jsn(user) + ' -> ' + jsn(out));
			if (out.err) {
				return res.send(jsn({ err: jsn(out.err) }));
			}
			res.send(jsn({}));
		});
	});

	app.post('/ajax/admin/remove', function(req, res) {
		fib(function() {
			log('app.get /ajax/admin/remove');
			var loginUser = req.session.loginUser;
			if (!loginUser || !loginUser.isAdmin) {
				return res.send(jsn({ err: 'Access denied' }));
			}
			var condition = JSON.parse(dec(req.body.condition));
			if (!condition) {
				return res.send(jsn({ err: 'Missing parameters' }));
			}
			var out = yld(Model.remove(condition, cbk('err')));
			log(jsn(condition) + ' -> ' + jsn(out));
			if (out.err) {
				return res.send(jsn({ err: jsn(out.err) }));
			}
			res.send(jsn({}));
		});
	});

  //--end of define app.get and app.post


  self.init = function(ModelIn, senderPasswordIn, noSendIn) {
	log('init Model=%d', ModelIn ? 1 : 0);
	Model = ModelIn;
	senderEmail = 'itisregister@outlook.com';
	senderPassword = senderPasswordIn;
	noSend = noSendIn;
  }

  self.nameChk = function(name, cbErr) {
	log('nameChk name=%s', name);
	if (!name || name == '') {
		return cbErr('name is empty');
	}
	Model.find({ _id : name }, function(findErr, rows) {
		log('nameChk Model.find _id:%s findErr=%j rows.length=%d', name, findErr, rows.length);
		cbErr(!findErr && rows.length > 0 ?'name exists' : null);
	});
  }

  self.passChk = function(pass) {
	log('passChk');
	if (!pass || pass == '') {
		return { err: 'password is empty' };
	}
	if (pass.length < 6) {
		return { err: 'password is less than 6 characters' };
	}
	if (pass.match(/[^ -~]/)) {
		return { err: 'password contains non basic ASCII characters' };
	}
	if (!pass.match(/[0-9]/)) {
		return { err: 'password does not contain any digit' };
	}
	if (!pass.match(/[A-Z]/)) {
		return { err: 'password does not contain any upper letter' };
	}
	if (!pass.match(/[a-z]/)) {
		return { err: 'password does not contain any lower letter' };
	}
	return { err: null }
  }

self.captchaImg = function(session, cbErrImg) {
	log('captchaImg session=%j', session);
	captchaGen(session);
	var canvas = new Canvas(200, 150);
	var ctx = canvas.getContext('2d');
	var len = session.captcha.code.length;
	for (var pos = 0; pos < len; pos++) {
		var ch = session.captcha.code.charAt(pos);
		var form = session.captcha.form[pos];
		ch = form.upper ? ch.toUpperCase() : ch;
		ctx.font = form.size + 'px Arial';
		ctx.lineWidth = form.line;
		ctx.rotate(form.angle);
		ctx.strokeText(ch, form.x, form.y);
	}
	ctx.save();
	canvas.toBuffer(cbErrImg);
}

function captchaGen(session) {
	if (session.captcha) {
		return;
	}
	captchaRegen(session);
}

function captchaRegen(session) {
	var code = '';
	var num = Math.random();
	for (var pos = 0; pos < 6; pos++) {
		num *= 26;
		var one = Math.floor(num);
		num -= one;
		code += String.fromCharCode(97 + one);
	}
	var form = [];
	for (var pos = 0; pos < code.length; pos++) {
		form.push({
			upper : random(0, 1),
			size : random(25, 75),
			line : random(1, 3),
			angle : randomFloat(0, 0.1),
			x : 200 / (code.length + 1) * (pos + 1) + random(-10, 10),
			y : 75 + random(-20, 20)
		});
	}
	session.captcha = { code: code, form: form };
}

self.captchaRegen = function(session) {
	log('captchaRegen session=%j', session);
	captchaRegen(session);
}

self.captchaChk = function(session, input) {
	log('captchaChk session=%j input=%s', session, input);
	if (!input) {
		return { err: 'captcha input is empty' };
	}
	if (!session.captcha) {
		return { err: 'captcha truth is not ready' };
	}
	if (input.toLowerCase() != session.captcha.code) {
		return { err: 'captcha code mismatch' };
	}
	return { err: null };
}

var mailTransport;
self.emailSend = function(session, email, cbErr) {
	log('mailTransporter.sendMail session=%s email=%s', session, email);
	if (!email || email == '') {
		return cbErr('email is empty');
	}

	Model.find({ emailVerified : email }, function(findErr, rows) {
		log('emailSend Model.find email:%s findErr=%j rows.length=%d', email, findErr, rows.length);
		if (!findErr && rows.length > 0) {
			return cbErr('email has been used by another user');
		}
		mailGen(session, email);
		mailTransport = mailTransport || nodemailer.createTransport({
			service: 'Outlook',
			auth: {
				user: senderEmail,
				pass: senderPassword
			}
		});
		var mailOptions = {
			from: 'I Travel I Show <itravelishow@outlook.com>',
			to: email,
			subject: 'Email verification code from I Travel I Show',
			text: 'Your email verification code is as below, please use it to continue your signup process: ' + session.mail.code,
			html: 'Your email verification code is as below,<br/>please use it to continue your signup process:<br/>' + session.mail.code
		};
		log('mailTransport.sendMail email=%s code=%s', email, session.mail.code);
		if (noSend) {
			return cbErr(null);
		}
		mailTransport.sendMail(mailOptions, function(error, info){
			log('mailTransporter.sendMail error=%j info=%j', error, info);
			if (error != null && error.responseCode == '550'){
			  log('Send email get error 550, means mail server think this email is a spam');
			  error = null;
			}
			return cbErr(error);
		});
	});
}

self.emailCodeChk = function(session, emailCode) {
	log('emailCodeChk session=%j emailCode=%s', session, emailCode);
	if (!emailCode) {
		return { err: 'email code input is empty' };
	}
	if (!session.mail) {
		return { err: 'email code is not ready' };
	}
	if (emailCode != session.mail.code) {
		return { err: 'email verification code mismatch' };
	}
	return { err: null };
}

function mailGen(session, email) {
	if (session.mail && session.mail.email == email) {
		return;
	}
	var code = '';
	var num = Math.random();
	for (var pos = 0; pos < 6; pos++) {
		num *= 10;
		var one = Math.floor(num);
		num -= one;
		code += String.fromCharCode(48 + one);
	}
	session.mail = { email: email, code: code };
}

self.signup = function(session, name, pass, captcha, emailCode, cbErrUser) {
	log('signup session=%j name=%s captcha=%s emailCode=%s', session, name, captcha, emailCode);
	var passChkOut = self.passChk(pass);
	if (passChkOut.err) {
		return cbErrUser(passChkOut.err);
	}
	var captchaChkOut = self.captchaChk(session, captcha);
	if (captchaChkOut.err) {
		return cbErrUser(captchaChkOut.err);
	}
	var emailCodeChkOut = self.emailCodeChk(session, emailCode);
	if (emailCodeChkOut.err) {
		return cbErrUser(emailCodeChkOut.err);
	}
	self.nameChk(name, function(nameChkErr) {
		if (nameChkErr) {
			return cbErrUser(nameChkErr);
		}
		var email = session.mail.email;
		Model.find({ emailVerified : email }, function(findErr, rows) {
			log('signup Model.find email:%s findErr=%j rows.length=%d', email, findErr, rows.length);
			if (!findErr && rows.length > 0) {
				return cbErrUser('email has been used by another user');
			}
			bcrypt.hash(pass, 10, function(hashErr, passwordEncrypted) {
				if (hashErr) {
					cbErrUser('fail to encrypt password for:' + jsn(hashErr));
				}
				var model = new Model({
					_id : name,
					dispname: name,
					passwordEncrypted: passwordEncrypted,
					emailVerified: email
				});
				log('Model.save name=%s email=%s', name, email);
				model.save(function(saveErr, row) {
					if (saveErr) {
						return cbErrUser('fail to save new user for:' + jsn(saveErr));
					}
					cbErrUser(null, row);
				});
			});
		});	
	});
}

  self.signin = function(name, pass, cbErrUser) {
	log('signin name=%s', name);
	Model.find({ _id: name }, function(findErr, rows) {
		log('signin Model.find user id:%s findErr=%j rows.length=%d', name, findErr, rows.length);
		if (findErr || rows.length <= 0) {
			return cbErrUser('user id does not exist');
		}
		bcrypt.compare(pass, rows[0].passwordEncrypted, function(compareErr, isMatched) {
			if (compareErr) {
				return cbErrUser('fail to verify the password for:' + compareErr);
			}
			if (!isMatched) {
				return cbErrUser('password is not correct');
			}
			cbErrUser(null, rows[0]);
		});
	});
  }

	self.users = function(page, cbErrUsers) {
		log('users');
		Model.find({}).sort({'_id':-1}).skip(100 * page).limit(100).exec(cbErrUsers);
	}

  function jsn(obj) {
	return JSON.stringify(obj);
  }
  function log(args) {
	var fmtstr = utilnode.format.apply(this, arguments);
	util.log('[signup] '+ fmtstr);
  }
  function random(min, max) {
	return Math.floor(min + Math.random() * (max + 1 - min));
  }
  function randomFloat(min, max) {
	return min + Math.random() * (max - min);
  }
  function dec(encoded) {
        return encoded ? decodeURIComponent(escape(fromHex(encoded))) : null;
  }
  function fromHex(encoded) {
    var out = '';
    for (var pos = 0; pos + 1 < encoded.length; pos += 2) {
      out += String.fromCharCode(parseInt(encoded.substring(pos, pos + 2), 16));
    }
    return out;
  }
}//End of constrctor: signup(app)

