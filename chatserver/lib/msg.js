
// Export a convenience function that creates an instance of message manager
module.exports = function(dgmfs, cgmgr) {
  return new msgmgr(dgmfs, cgmgr);
}


// The actual constructor function, which is also a closure 
// holding all the private state information

function msgmgr(dgmfs, cgmgr) {

  // Reference to "this" that won't get clobbered by some other "this"
  var self = this;
  
  // Define log function
  var util = require("../util/util.js");
  var utilnode = require("util");
  function log(args){
    var fmtstr = utilnode.format.apply(this, arguments);
    util.log("[msg] "+ fmtstr);
  }

  // Require async library
  var async = require('async');

  // Private state variables
  var schema = dgmfs.mongoose.Schema;
  var mongoose = dgmfs.mongoose;
  var cgmgr = cgmgr;
  var usermgr = cgmgr.usermgr;

  // Define message schema
  var msgschema = new schema({
    msg: String,
    recvtime: { type : Date, default: Date.now }, // receive time
    dlvrtime: { type: Date }, //deliver time
    chatgroup: { type : String, ref: 'chatgroup' },
    from: { type: String, ref: 'user' },
    sent: Boolean //whether delivered or not
    },
    {collection: 'message'}
  );

  self.msgschema = msgschema;

  // Define message model
  var msgmodel = dgmfs.mongoose.model('message', msgschema);
  self.msgmodel = msgmodel;

  // Pre save 'message'
  msgschema.pre('save', function (next) {

    var self = this;
    log("Save: try to find chatgroup %s", self.chatgroup)
    cgmgr.cgmodel.findById(self.chatgroup, function(err, cg){
      if(cg == null || err) {
        return next(utilnode.format("Unable to find chatgroup %s", self.chatgroup));
      } else {
        if (cg.members.indexOf(self.from) == -1){
          return next(utilnode.format("from user {%s} does not belong to chatgroup {%s}",
               self.from, self.chatgroup));
        } else {
          next();
        }
      }
    });
  });

  // Define save function for message
  function saveMessage(msg, cgid, from, callback){

    log("save msg {%s} send to chatgroup {%s} from user {%s}", msg, cgid, from);
    var m = new msgmodel({
      msg: msg,
      chatgroup: cgid,
      from: from,
      sent: false});
    m.save(function(err, savedmsg){
      callback(err, savedmsg);
    });
  };

  // Export saveMessage function
  self.saveMessage = function(msginfo, callback){

    // try to add message
    saveMessage(msginfo.msg, msginfo.cgid, msginfo.from, function(err, savedmsg){
      if (err) {
        log(err);
        return callback(null);
      }
      log("Save msg successfully.");
      callback(savedmsg._id);
    });
  };

  // Export updateMessage function
  // 1- update sent flag to true
  self.updateMessage = function(msgid, callback){
    var query = {'_id': msgid};
    msgmodel.findOneAndUpdate(query, {sent: true}, {upsert:false}, function(err, doc){
        if (err) return log('Failed to update message {%s}', err);
        return log('update message {%s} sent flag to true', msgid);
    });
  };

  // function - get message send to a chatgroup
  function getmsgOfCg(cg, callback){

        log("get msg for chatgroup %s", cg);

        async.parallel([
          function(callback){
            // find chatgroup by id
            cgmgr.cgmodel.findOne({_id:cg}, callback);
          },
          function(callback){
            // find all messages send to the chatgroup
            // TODO: Limit message count, sort by time,
            msgmodel.find({chatgroup:cg}, callback);
          }
        ],
        function(err, results) {
          // results is now [cg, msgs]
          if (err) {
            log("find msg for chatgroup {%s} error {%s}", cg, err)
            return callback(null, null);
          }
 
          if (results == null || results[0] == null) {
            log("unable to find chatgroup {%s}", cg);
            return callback(null, null);
          }

          // callback message data
          var msgdata = {cg: results[0], msgs: results[1]};
          callback(null, msgdata);
        });
  }

  // Export getMessage function
  // Get all message;
  // V2: get only new(pending) messages
  self.getMessage = function(userid, callback){
    log("___get message for user {%s}", userid);

    usermgr.getChatgroup({_id: userid}, function(chatgroups){
      if (!chatgroups.length)
        return callback("unable to find chatgroup");

      log("Found %d chatgroups for user %s", chatgroups.length, userid);

      // map array of cg, query msgs of each chatgroup
      async.map(chatgroups, getmsgOfCg, callback);
    });
  };
}

