var util = require('util');
var geo = require('./geo.js');

// Export a convenience function that creates an instance
module.exports = function(dgmfs) {
  return new usermgr(dgmfs);
}


// The actual constructor function, which is also a closure 
// holding all the private state information

function usermgr(dgmfs) {

  // Reference to "this" that won't get clobbered by some other "this"
  var self = this;

  // Private state variables
  var schema = dgmfs.mongoose.Schema;

  // Define log function
  var util = require("../util/util.js");
  var utilnode = require("util");
  function log(args){
    var fmtstr = utilnode.format.apply(this, arguments);
    util.log("[user] "+ fmtstr);
  }

  // Define user schema
  var userschema = new schema({
    // TODO: avatar: ...
    _id:String, //itisid
    dispname: String,
    type: {type:String, enum:['guide', 'tourist'], default:'tourist'},
    hasLoc: Boolean,
    location: { type:{type:String, default:"Point"}, coordinates: [Number] },
    status: String,
    group: [{name: String, members: [{userid:{type: String, ref: 'user'}, displayname: String}]}],
    chatgroup: [{type : String, ref: 'chatgroup'}],
    passwordEncrypted: String,
    emailVerified: String,
    avatar: String,
    isAdmin: Boolean,
    langid: String //language id
    },
    {collection: 'user'}
  );

  // define index for location field
  userschema.index({ location: "2dsphere" });

  // export user schema
  self.userschema = userschema;

  // Define user model
  var usermodel = dgmfs.mongoose.model('user', self.userschema);
  self.usermodel = usermodel;

  // Pre save 'user'
  userschema.pre('save', function (next) {

    var self = this;
    log('pre save user {%s}', self._id);
    geo.preSave(self);
    if (!self.isNew) // Modify existing user TODO: remove
    {
      log("Modify existing user document");
      return next();
    }

    usermodel.find({_id : self._id}, function (err, docs) {
        if (!docs.length)
          return next();

        next(utilnode.format('user id %s already exist', self._id));
    });
  });

  // Implement create user 
  function createUserImpl(uid, displayname, callback){

    var u = new usermodel({
      _id: uid,
      dispname:displayname,
      location:{ type : "Point", coordinates : [50, 30] } // TODO: get initial geo coordinates from client
    });

    u.save(function(err){
      callback(err);
    });
  };

  // Export createUser function
  self.createUser = function(udata){

    // Try to create user
    createUserImpl(udata._id, udata.dispname, function(err){
      if (err)
        return log('create user %s failed {%s}', udata._id, err);
 
      log("create user {%s} success.", udata._id);
    });
  };

  // export addChatgroup function
  self.addChatgroup = function(udata){
    usermodel.findById(udata._id, function(err, user) {
      if (err || user == null || user.length == 0)
        return log("Unable to find user {%s}", udata._id);

      log("chatgroup count %d", user.chatgroup.length);
      if (user.chatgroup.indexOf(udata.cgid) > -1)
        return log('chatgroup %s already in user %s chatgroups');

      log("add chatgroup %s to user %s", udata.cgid, udata._id);
      user.chatgroup.push(udata.cgid);
      user.save(function(err){
        if (err)
          return log(err);
        log("Add chatgroup %s to user %s succeeded", udata.cgid, udata._id);
      });
    });
  };

  // export getChatgroup function
  self.getChatgroup = function(udata, callback){
    log('Try to find chatgroup for user %s', udata._id);

    usermodel.findById(udata._id, function(err, user) {
      log('find user callback');
      if (err || user == null || user.length==0) {
        log("Failed to find the user %s", udata._id);
        callback([]);
      } else {
        log("User %s has %d chatgroups", udata._id, user.chatgroup.length);
        callback(user.chatgroup);
      }
    });
  };

  // export updateAvatar function
  self.updateAvatar = function(udata){
    usermodel.findById(udata._id, function(err, user) {
      if (err || user == null || user.length == 0)
        return log("Unable to find user {%s}", udata._id);
      if (user.avatar){
        log("old avartar image %s", user.avatar);
      }
      user.avatar = udata.avatar;
      user.save(function(err){
        if (err)
          return log(err);
        log("update avatar {%s} to user {%s} succeeded", udata.avatar, udata._id);
      });
    });
  };
}

