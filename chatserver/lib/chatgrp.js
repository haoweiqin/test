

// Export a convenience function that creates an instance of session manager
module.exports = function(dgmfs, usermgr) {
  return new cgmgr(dgmfs, usermgr);
}


// The actual constructor function, which is also a closure 
// holding all the private state information

function cgmgr(dgmfs, usermgr) {

  // Reference to "this" that won't get clobbered by some other "this"
  var self = this;
  var umgr = usermgr;
  self.usermgr = usermgr;

  // Private state variables
  var schema = dgmfs.mongoose.Schema;
  var mongoose = dgmfs.mongoose;

  // Define log function
  var util = require("../util/util.js");
  var utilnode = require("util");
  function log(args){
    var fmtstr = utilnode.format.apply(this, arguments);
    util.log("[chatgrp] "+ fmtstr);
  }

  // Define chat group schema
  var cgschema = new schema({
    _id: String, // combination of sorted member id
    displayName: String,
    lastmsgtime: { type : Date, default: Date.now },
    members: [{ type : String, ref: 'user' }],
    },
    {collection: 'chatgroup'}
  );

  self.cgschema = cgschema;

  // Define chat group model
  var cgmodel = dgmfs.mongoose.model('chatgroup', cgschema);
  self.cgmodel = cgmodel;

  // Pre save 'chatgroup'
  cgschema.pre('save', function (next) {
    var self = this;
    cgmodel.find({_id : self._id}, function (err, docs) {
        if (!docs.length){
            next(null, self);
        }else{
            log('chatgroup {%s} exists.', self._id);
            next(new Error("chatgroup exists!"));
        }
    });
  });

  function generateCgjson(users) {

    var cgid = '';
    var userids = [];
    users.forEach(function(user) {
      log('user id %s', user._id);
      userids.push(user._id);
    });

    // build chat group id
    // by concat sorted userids
    userids.sort();
    userids.forEach(function(userid) {
      cgid += userid;
    });

    log("chagroup id %s", cgid);
    return {cgid:cgid, userids:userids};
  }

  // Define impl function for creating a new chatgroup
  function createNewChatgroup(cgjson, callback){

    var cg = new cgmodel({
      _id: cgjson.cgid,
      members: cgjson.userids});

    cg.save(function(err, chatgroup){
      callback(err, chatgroup);
    });
  };

  // Define Chatgroup creation function
  // and add chatgroup to users
  function create_addNewChatgroup(cgjson, callback){

    log('create new chatgroup {%s} and add it to users', cgjson.cgid);
    // create chatgroup
    createNewChatgroup(cgjson, function(err, cg){
        if (err){
          callback(err);
        } else {
          cgjson.userids.forEach(function(userid) {
            log("Add chatgroup {%s} to user {%s}", cg._id, userid);
            var udata = {_id: userid, cgid: cg._id};
            umgr.addChatgroup(udata); //TODO: Error handling
          });
          log("create chatgroup {%s} successfully", cg._id);
          callback(null, cg._id);
        }
    });
  }

  // Export createChatgroup function
  self.createChatgroup = function(users){

    var cgjson = generateCgjson(users);
    log('create chatgroup with id {%s}', cgjson.cgid);

    // create chatgroup and add cg to users
    create_addNewChatgroup(cgjson, function(err, cgid){
      if (err){
        log("createChatgroup failed:", err);
      }
    });
  };

  // Ensure chatgroup by _id
  self.ensureChatgroup = function(users, callback){

    var cgjson = generateCgjson(users);
    log('Ensure chatgroup {%s}', cgjson.cgid);

    // try to find chatgroup first
    cgmodel.find({_id : cgjson.cgid}, function (err, cgs) {
      if (cgs.length == 1){
        log('...chatgroup {%s} exists.', cgjson.cgid);
        return callback(null, cgjson.cgid);
      }
      if (cgs.length > 1){
        return callback('More than one chatgroup has same id');
      }
      if (err) {
	log("Find chatggroup {%s} with error %s. Ignored.", cgjson.cgjd, err)
      }
      // create chatgroup
      create_addNewChatgroup(cgjson, callback);
    });
  };
}

