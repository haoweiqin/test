var MaxDistanceDegreeMin = 0.00001; // ~ 1 meter
var MaxDistanceDegreeMax = 45; // ~5,000 kilometer

var DegreeToMeter = 1000000 / 9; // 111,111.1111 meter per degree

var Locatable;
module.exports.init = function(mongoose) {
	//dev:log("geo.init");
	var locatableSchema = mongoose.Schema({
		id : String,
		location : {
			type: {
				type: String,
				default: "Point"
			},
			coordinates: [Number]
		},
		guide : Boolean,
		tourist : Boolean
	});
	locatableSchema.index({ location: "2dsphere" });
	Locatable = mongoose.model("locatable", locatableSchema);
};

module.exports.update = function(id, long, lat, prop, cbErr) {
	//dev:log("geo.update id=" + id + " long=" + long + " lat=" + lat + " prop=" + jsn(prop));
	if (!Locatable) {
		return cbErr(new Error("geo.update fail for not initialized"));
	}
	Locatable.remove({ id : id }, function(err) {
		if (err) {
			return cbErr(new Error("geo.update fail at remove err=" + err));
		}
		var locatable = new Locatable({
			id: id,
			location : { coordinates : normalizeCoord(long, lat) },
			guide : prop.guide,
			tourist : prop.tourist});
		locatable.save(function(err, row) {
			if (err) {
				return cbErr(new Error("geo.update fail at save err=" + err));
			}
			cbErr();
		});
	});
}

function normalizeCoord(long, lat) {
	var lz = long + 180;
	var lq = lz - Math.floor(lz / 360) * 360 - 180;
	var pz = lat + 90;
	var pq = pz - Math.floor(pz / 180) * 180 - 90;
	return [ lq, pq ];
}

module.exports.search = function(long, lat, maxDistanceDegree, query, limit, cbErrResults) {
	if (!Locatable) {
		return cbErrResults(new Error("geo.search fail for not initialized"));
	}
	/*dev*/log("geo.search long=" + long + " lat=" + lat + " maxDegree=" + maxDistanceDegree + " query=" + jsn(query) + " limit=" + limit);
	var maxDistanceMeter = Math.max(MaxDistanceDegreeMin, Math.min(MaxDistanceDegreeMax, maxDistanceDegree)) * DegreeToMeter;
	/*dev*/log("geo.search maxDistanceMeter=" + maxDistanceMeter);
	Locatable.geoNear({
			type : "Point",
			coordinates : normalizeCoord(long, lat)
		}, {
			spherical : true,
			maxDistance : maxDistanceMeter,
			num: limit,
			query : query
		}, function(err, results, stats) {
			if (err) {
				return cbErrResults(new Error("geo.search fail at Locatable.geoNear " + err));
			}
			cbErrResults(err, results);
		});
}

module.exports.delete = function(id, cbErr) {
	if (!Locatable) {
		return cbErr(new Error("geo.delete fail for not initialized"));
	}
	Locatable.remove({ id : id }, function(err) {
		if (err) {
			return cbErr(new Error("geo.delete fail at Locatable.remove " + err));
		}
		cbErr();
	});
}

// ### helper
function log(msg) {
	console.log(new Date().toJSON() + " " + msg);
}
function jsn(obj) {
	return JSON.stringify(obj);
}
