
//NOTE: to test/debug index.js on different port
// export IPORT environment variable from shell before run node 'index.js'
// for example: export IPORT=8080
// so far only 8080 or 3030 are supported, default is 3030 (run by official server)

//dev:debugger; // for Node-Inspector

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var chatserver = require('./lib/chatsrv.js')(io);
var geo = require("./geo.js");

function log(msg) {
    console.log(new Date().toJSON() + " " + msg);
}

function jsn(obj) {
	return JSON.stringify(obj);
}

// Mongoose import
var mongoose = require('mongoose');
// Mongoose connection to MongoDB (ted/ted is readonly)
mongoose.connect('mongodb://127.0.0.1/itisdb', function (error) {
	//dev:log("mongoose.connect error=" + error);
    if (error) {
        log("mongoose.connect error" + error);
    }
});

// Mongoose Schema definition
var Schema = mongoose.Schema;
var userSchemaParam = {
    name: String,
    status: String
};
geo.updateSchemaParam(userSchemaParam);
var UserSchema = new Schema(
    userSchemaParam,
    {collection: 'userorg'}
);
var indexParam = {};
geo.updateIndexParam(indexParam);
UserSchema.index(indexParam);

// Mongoose Model definition
var UserModel = mongoose.model('userlegacy', UserSchema);

// Update User
function updateUser(user, cb){
	//dev:log("updateUser user=" + jsn(user));
    UserModel.find({name : user.name}, function (err, docs) {
    	//dev:log("UserModel.find user.name=" + user.name + " err=" + err + " docs.length=" + docs.length);
        if (docs.length){
            cb('Name exists already',null);
        }else{
            user.save(function(err){
            	//dev:log("user.save user.name=" + user.name + " err=" + err);
                cb(err,user);
            });
        }
    });
}

// Save user option#2
UserSchema.pre('save', function (next) {
	//dev:log("UserSchema.pre save name=" + this.name);
    var self = this;
    geo.preSave(self);
    UserModel.find({name : self.name}, function (err, docs) {
    	//dev:log("UserModel.find self.name=" + self.name + " err=" + err + " docs.length=" + docs.length);
        if (!docs.length){
            next();
        }else{                
            log('user exists: ',self.name);
            next(new Error("User exists!"));
        }
    });
}) ;


//app.get('/', function(req, res){
//  res.send('<h1>Hello world</h1>');
//:wq
//});

// rooms which are currently available in chat
var rooms = ['room1','room2','room3'];

var portnumber=process.env.IPORT
if (portnumber){
    } else { portnumber = 3030; }

// TODO: delete chatroom.html
app.get('/test', function(req, res){
	//dev:log("app.get /");
  res.sendFile(__dirname + '/chatroom.html');
});

// Test the chat namespace
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/users', function(req, res){
	//dev:log("app.get /users");
   user.find({}, function (err, docs) {
   		//dev:log("user.find err=" + err);
        res.json(docs);
    });
});

// http://52.26.166.250:3030/guides?long=120&lat=50&deg=10
app.get("/guides", function(req, res) {
	//dev:log("app.get /guides");
	var long = parseFloat(req.query.long);
	var lat = parseFloat(req.query.lat);
	var deg = parseFloat(req.query.deg);
	//dev:log("app.get /guides long=" + long + " lat=" + lat + " degree=" + deg);
	if (isNaN(long) || isNaN(lat) || isNaN(deg)) {
		return res.send(jsn(new Error("Wrong format request")));
	}
	geo.search(UserModel, long, lat, deg, {}, 10, function(err, results) {
		if (err) {
			return res.send(jsn(err));
		}
		res.send(jsn(results));
	});
});

// http://52.26.166.250:3030/report?usr=abc&loc=1&long=120&lat=50&del=0
app.get("/report", function(req, res) {
	//dev:log("app.get /report");
	var usr = req.query.usr;
	var hasLoc = "1" == req.query.loc;
	var long = parseFloat(req.query.long);
	var lat = parseFloat(req.query.lat);
	var del = "1" == req.query.del
	//dev:log("app.get /report usr=" + usr + " hasLoc=" + hasLoc + " long=" + long + " lat=" + lat + " del=" + del);
	if (!usr || isNaN(long) || isNaN(lat)) {
		return res.send(jsn(new Error("Wrong format request")));
	}
	if (del) {
		UserModel.remove({ name: usr }, function (removeErr) {
			if (removeErr) {
				return res.send(jsn(removeErr));
			}
			return res.send({ ok : 1});
		});
		return;
	}
	var row = {};
	geo.updateLocation(row, hasLoc, long, lat);
	UserModel.update({ name: usr}, row, { upsert: true }, function (updateErr, rawResp) {
		if (updateErr) {
			return res.send(jsn(updateErr));
		}
		return res.send({ ok: 1 });
	});
});

// usernames which are currently connected to the chat
var usernames = {};

//dev:var sn=0
io.on('connection', function(socket){
  //dev:var id=sn++;
  //dev:function logio(msg) {
  //dev:    log("io#" + id + " " + msg);
  //dev:}
  log('a user connected');
  socket.on('disconnect', function(){
    log('user disconnected');
  });

  socket.on('ChatMsg', function(msg){
    log('ChatMsg message=' + msg);
    //dev:logio("emit ChatMsg " + msg);
    io.emit('ChatMsg', msg);
  });
  
  // when the client emits 'adduser', this listens and executes
   socket.on('adduser', function(userdata){
        //dev:logio("adduser " + jsn(userdata));
		// store the username in the socket session for this client
		socket.username = userdata.username;
		// store the room name in the socket session for this client
		var roomcur = userdata.room;
		if (rooms.indexOf(roomcur) <= -1) {
			rooms.push(roomcur);
		}

		socket.room = roomcur;
		// add the client's username to the global list
		usernames[userdata.username] = userdata.username;
		// send client to room 1
		socket.join(roomcur);
		// echo to client they've connected
		//dev:logio("emit updatechat SERVER you have connected to " + roomcur);
		socket.emit('updatechat', 'SERVER', 'you have connected to '+roomcur);
		// echo to room 1 that a person has connected to their room
		//dev:logio("emit-broadcast " + roomcur + " updatechat SERVER " + userdata.username + " has connected to this room");
		socket.broadcast.to(roomcur).emit('updatechat', 'SERVER', userdata.username + ' has connected to this room');
		//dev:logio("emit updaterooms rooms=" + jsn(rooms) + " roomcur=" + roomcur);
		socket.emit('updaterooms', rooms, roomcur);

                //user.find({}, function (err, docs) {
                //      socket.emit('updateusers', docs);
                var u = new UserModel({
                   name:userdata.username,
                   status:"online"
                });
                u.save(function(err){
                    //dev:logio("u.save err=" + err);
              		if (err)
				log(err);
			else
				log("save user: " + userdata.username + " successfully.");
		});
        });

	// when the client emits 'sendchat', this listens and executes
	socket.on('sendchat', function (data) {
		//dev:logio("sendchat data=" + data);
		// we tell the client to execute 'updatechat' with 2 parameters
		//dev:logio("emit-in-room " + socket.room + " updatechat socket.username=" + socket.username + " data=" + data);
		io.sockets.in(socket.room).emit('updatechat', socket.username, data);
	});

	socket.on('switchRoom', function(newroom){
		//dev:logio("switchRoom newroom=" + newroom);
		// leave the current room (stored in session)
		socket.leave(socket.room);
		// join new room, received as function parameter
		socket.join(newroom);
		//dev:logio("emit updatechat SERVER you have connected to " + newroom);
		socket.emit('updatechat', 'SERVER', 'you have connected to '+ newroom);
		// sent message to OLD room
		//dev:logio("emit-broadcast " + socket.room + " updatechat SERVER " + socket.username + " has left this room");
		socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username+' has left this room');
		// update socket session room title
		socket.room = newroom;
		//dev:logio("emit-broadcast " + newroom + " updatechat SERVER " + socket.username + " has joined this room");
		socket.broadcast.to(newroom).emit('updatechat', 'SERVER', socket.username+' has joined this room');
		//dev:logio("emit updaterooms rooms=" + jsn(rooms) + " newroom=" + newroom);
		socket.emit('updaterooms', rooms, newroom);
	});

	socket.on('searchGuides', function(data) {
		//dev:logio("searchGuides data=" + jsn(data));
	});

	// when the user disconnects.. perform this
	socket.on('disconnect', function(){
		//dev:logio("disconnect");
		// remove the username from global usernames list
		delete usernames[socket.username];
		// update list of users in chat, client-side
		//dev:logio("emite updateusers " + jsn(usernames));
		io.sockets.emit('updateusers', usernames);
		// echo globally that this client has left
		//dev:logio("emit-broadcast all updatechat SERVER " + socket.username + " has disconnected");
		socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
		socket.leave(socket.room);
	});

});


//dev:app.get('/quit', function(req, res){
//dev:  log("app.get /quit");
//dev:  res.send("Quitting");
//dev:  setTimeout(function() {
//dev:    var fs = require("fs");
//dev:    fs.unlinkSync("./geo_dev.js");
//dev:    fs.unlinkSync("./index_dev.html");
//dev:    fs.unlinkSync("./index_dev.js");
//dev:    log("exiting");
//dev:    process.exit(0);
//dev:  }, 500);
//dev:});

http.listen(portnumber, function(){
  log('listening on *:'+portnumber);
  log('NOTE: you can change the port number by run \'export IPORT=<XXX>\' to change the port number');
});



