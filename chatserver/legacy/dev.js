(function() {
	var files = [ "index.js", "index.html", "geo.js" ];
	var only_change_ip = null;
	process.argv.forEach(function (arg) {
		if (arg == "oci") {
			only_change_ip = true;
		}
	});
	var fs = require("fs");
	for (var i = 0; i < files.length; i++) {
		var src = files[i];
		var tgt = src.replace(/\./, "_dev.");
		console.log(src + " -> " + tgt);
		var input = fs.readFileSync(src, "utf8");
		var output = input.replace("52.26.166.250", "127.0.0.1");
		if (only_change_ip) {
			console.log("only change ip");
			fs.writeFileSync(tgt, output);
			continue;
		}
		output = output.replace(/\/\/dev:/g, "")
			.replace(/index\.html/g, "index_dev.html")
			.replace(/geo\.js/g, "geo_dev.js")
			.replace("<!--dev", "")
			.replace("dev-->", "");
		fs.writeFileSync(tgt, output);
	}
})();
require("./index_dev.js");
