
//NOTE: to test/debug index.js on different port
// export IPORT environment variable from shell before run node 'index.js'
// for example: export IPORT=8080
// so far only 8080 or 3030 are supported, default is 3030 (run by official server)

//dev:debugger; // for Node-Inspector

var fs = require('fs');
var config = JSON.parse(fs.readFileSync(__dirname + '/config.json'));
var express = require('express');
var app = express();
var host = config.host;
var multer = require('multer');

// Read port number from environment variable
var port  = process.env.IPORT || config.port || 3030;

var https = require('https');

// Read key and cert from file
var keypem = __dirname+"/itravelishow_com.key.pem";
var certpem = __dirname+"/itravelishow_com.crt";
var options = {
  key: fs.readFileSync(keypem),
  cert: fs.readFileSync(certpem)
};

var httpsSrv = https.createServer(options, app);
var io = require('socket.io').listen(httpsSrv);
var cookie = require("cookie");

var chatsrv = require('./lib/chatsrv.js')(io);
var usermgr = chatsrv.usermgr;
var usermodel = usermgr.usermodel;
var msgtmgr = chatsrv.msgtmgr;

// Auhtorization code logic
var cookieParser = require('cookie-parser');
var secretsio = 'itis@wxwx';
app.use(require('express-session')({secret: secretsio, resave:true, saveUninitialized:true, key: 'express.sid'}));

io.set('authorization', function (handshakeData, accept) {
  if (handshakeData.headers.cookie) {
    handshakeData.cookie = cookie.parse(handshakeData.headers.cookie);
    handshakeData.sessionID = cookieParser.signedCookie(handshakeData.cookie['express.sid'], secretsio);
    log('cookie.express.sid:' +  handshakeData.cookie['express.sid']);
    log('sessionID: ' + handshakeData.sessionID);
    if (handshakeData.cookie['express.sid'] == handshakeData.sessionID) {
      log('cookie is invalid');
      return accept('Cookie is invalid.', false);
    }
  } else {
    log('no cookie transmitted');
    return accept('No cookie transmitted.', false);
  }
  log('authorized')
  accept(null, true);
});

//
// EJS is CanJS's default template language, which provides
// live binding when used with Observes.
// EJS is very easy to use; you write the HTML you want to
// be in the template, along with a few magic tags where you want dynamic behavior.
require('ejs');
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));

// Initialize signup module
var signup = require('./lib/signup.js')(app);
var senderPasswordFile = config.senderPasswordFile;
chk(senderPasswordFile, 'missing senderPasswordFile config');
var senderPassword = fs.readFileSync(senderPasswordFile, 'utf8');
signup.init(usermodel, senderPassword, config.noSendEmail);

// Initialize geo module
var geo = require("./lib/geo.js");

function log(msg) {
  console.log(new Date().toJSON() + " " + msg);
}

function jsn(obj) {
  return JSON.stringify(obj);
}

function chk(condition, msg) {
  if (condition) {
    return;
  }
  log('fail at ' + msg + ' ' + jsn([].splice.call(arguments, 2)));
  process.exit(1);
}

// Test the chat namespace
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

// Test the image upload
app.get('/upload', function(req, res){
  res.sendFile(__dirname + '/upload.html');
});

app.get('/users', function(req, res){
  if (req.session == null || req.session.loginUser == null) {
    log('invalid request, please signin first');
    return res.redirect('/signin');
  }

  //dev:log("app.get /users");
  usermodel.find({}, function (err, docs) {
    //dev:log("user.find err=" + err);
    res.json(docs);
  });
});

app.get('/msgts', function(req, res){
  //if (req.session == null || req.session.loginUser == null) {
  //  log('invalid request, please signin first');
  //  return res.redirect('/signin');
  //}

  var langid = req.query.langid;
  if (!langid) {
    return res.send(jsn(new Error("Wrong format request")));
  }
  log('msg template request langid:' + langid);
  msgtmgr.getMsgTemplate(langid, function (err, msgts) {
    res.json(msgts);
  });
});

// http://52.26.166.250:3030/guides?long=120&lat=50&deg=10
app.get("/guides", function(req, res) {
  if (req.session == null || req.session.loginUser == null) {
    log('invalid request, please signin first');
    return res.redirect('/signin'); 
  }
	log("app.get /guides");
	var long = parseFloat(req.query.long);
	var lat = parseFloat(req.query.lat);
	var deg = parseFloat(req.query.deg);
	//dev:log("app.get /guides long=" + long + " lat=" + lat + " degree=" + deg);
	if (isNaN(long) || isNaN(lat) || isNaN(deg)) {
		return res.send(jsn(new Error("Wrong format request")));
	}
	geo.search(usermodel, long, lat, deg, {}, 10, function(err, results) {
		if (err) {
			return res.send(jsn(err));
		}
		res.send(jsn(results));
	});
});

// http://52.26.166.250:3030/report?usr=abc&loc=1&long=120&lat=50&del=0
app.get("/report", function(req, res) {
  if (req.session == null || req.session.loginUser == null) {
    log('invalid request, please signin first');
    return res.redirect('/signin');
  }

	//dev:log("app.get /report");
	var usr = req.query.usr;
	var hasLoc = "1" == req.query.loc;
	var long = parseFloat(req.query.long);
	var lat = parseFloat(req.query.lat);
	var del = "1" == req.query.del
	//dev:log("app.get /report usr=" + usr + " hasLoc=" + hasLoc + " long=" + long + " lat=" + lat + " del=" + del);
	if (!usr || isNaN(long) || isNaN(lat)) {
		return res.send(jsn(new Error("Wrong format request")));
	}
	if (del) {
		usermodel.remove({ name: usr }, function (removeErr) {
			if (removeErr) {
				return res.send(jsn(removeErr));
			}
			return res.send({ ok : 1});
		});
		return;
	}
	var row = {};
	geo.updateLocation(row, hasLoc, long, lat);
	usermodel.update({ name: usr}, row, { upsert: true }, function (updateErr, rawResp) {
		if (updateErr) {
			return res.send(jsn(updateErr));
		}
		return res.send({ ok: 1 });
	});
});

// http://<domain name>:3030/avatar?usrid=user1
app.get("/avatar", function(req, res) {
  if (req.session == null || req.session.loginUser == null) {
    log('invalid request, please signin first');
    return res.redirect('/signin');
  }

        var usrid = req.query.usrid;
        if (!usrid) {
          return res.send(jsn(new Error("Wrong format request")));
        }
	log('avatar request user:' + usrid);
        usermodel.findById(usrid, function (err, user) {
          if (err) {
            return res.send(jsn(err));
          }
	  if (!user) {
	    return res.send(jsn("unable to find user"));
	  }
	  log('user id' + user._id);
	  var avatar = user.avatar;
	  if (!(avatar)) avatar = "default.png";
	  log('user avatar: '+ avatar);
          return res.sendFile("/workdir/avatar/" + avatar);
        });
});

app.get('/map', function(req, res) {
  if (req.session == null || req.session.loginUser == null) {
    log('invalid request, please signin first');
    return res.redirect('/signin');
  }

  log('app.get /map');
  res.render('pages/map');
});

/*Configure the multer.*/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '/workdir/avatar'); // Absolute path. Folder must exist, will not be created for you.
  },
  filename: function (req, file, cb) {
    console.log(file);
    //console.log(file.filename);
    //cb(null, file.originalname);
    var oname = file.originalname;
    if (oname.indexOf('Optional(') === 0){
      var rightbracket = oname.indexOf(')');
      if(rightbracket != -1){
        oname = oname.substring(9, rightbracket);
      }
    }
    cb(null, oname);
    var fname = oname.substring(0, oname.lastIndexOf('.'));
    usermgr.updateAvatar({_id:fname, avatar:oname});
  }
});

var upload = multer({ storage:storage }).single('userPhoto');

app.post('/api/photo', function(req, res){

  if (req.session == null || req.session.loginUser == null) {
    log('invalid request, please signin first');
    return res.redirect('/signin');
  }

  upload(req, res, function(err){
    if (err) {
      console.log("upload photo failed:" + err);
      return;
    }
    console.log('upload photo succeeded');
  });
  console.log(req.body) // form fields
  console.log(req.files) // form files
  res.status(204).end()
});

//http.listen(port, function(){
httpsSrv.listen(port, function(){
  log('listening on *:'+port);
  log('NOTE: you can change the port number by run \'export IPORT=<XXX>\' to change the port number');
});

