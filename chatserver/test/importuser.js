var https = require('https');
var fs = require('fs');
var keypem = __dirname+'/../itravelishow_com.key.pem';
var certpem = __dirname+'/../itravelishow_com.crt';
var options = {
	key: fs.readFileSync(keypem),
	cert: fs.readFileSync(certpem)
};
var express = require('express');
var app = express();
var httpsSrv = https.createServer(options, app);
var io = require('socket.io').listen(httpsSrv);
var chatsrv = require('../lib/chatsrv.js')(io);
var usermgr = chatsrv.usermgr;
var usermodel = usermgr.usermodel;

var Fiber = require('fibers');
function fib() {
	if (arguments.length == 2) {
		new Fiber(arguments[1]).run(arguments[0]);
		return;
	}
	new Fiber(arguments[0]).run();
}
function cbk() {
	var fiber = Fiber.current;
	var names = [].splice.call(arguments, 0);
	return function() {
		var res = {};
		for (var pos = 0; pos < names.length; pos++) {
			res[names[pos]] = arguments[pos];
		}
		if (!fiber.itis_isYielding) {
			fiber.itis_syncRes = res;
			return;
		}
		fiber.run(res);
	}
}
function yld() {
	var fiber = Fiber.current;
	var syncRes = fiber.itis_syncRes;
	if (syncRes) {
		fiber.itis_syncRes = null;
		return syncRes;
	}
	fiber.itis_isYielding = true;
	var res = Fiber.yield();
	fiber.itis_isYielding = false;
	return res;
}

function log(msg) {
	console.log(new Date().toJSON() + " " + msg);
}
function jsn(obj) {
	return JSON.stringify(obj);
}
function end() {
	process.exit(0);
}
function chk(condition, msg) {
	if (condition) {
		return;
	}
	var detail = '';
	for (var pos = 2; pos < arguments.length; pos++) {
		var arg = arguments[pos];
		if (arg instanceof Object) {
			detail += ' ' + jsn(arg);
			continue;
		}
		detail += ' ' + arg;
	}
	log('fail at ' + msg + detail);
	process.exit(1);
}

fib(function() {
	log('hello');
	var input = fs.readFileSync('../resources/CityCoords.txt', 'utf8');
	var lines = input.split('\n');
	var guides = [];
	for (var row in lines) {
		var items = lines[row].split('\t');
		var long;
		var lat;
		if (items.length < 3
			|| isNaN(long = parseFloat(items[1]))
			|| isNaN(lat = parseFloat(items[2]))) {
			continue;
		}
		var user = {
			_id: 'Foo in ' + items[0],
			dispname : 'Foo in ' + items[0],
			type: 'guide',
			hasLoc: true,
			location: {
				coordinates: [ long, lat ]
			}
		};
		log(jsn(user));
		var out = yld(new usermodel(user).save(cbk('err')));
		log(jsn(out));
	}
	end();
});
