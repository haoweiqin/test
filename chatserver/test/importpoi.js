var https = require('https');
var fs = require('fs');

var Fiber = require('fibers');
function fib() {
	if (arguments.length == 2) {
		new Fiber(arguments[1]).run(arguments[0]);
		return;
	}
	new Fiber(arguments[0]).run();
}
function cbk() {
	var fiber = Fiber.current;
	var names = [].splice.call(arguments, 0);
	return function() {
		var res = {};
		for (var pos = 0; pos < names.length; pos++) {
			res[names[pos]] = arguments[pos];
		}
		if (!fiber.itis_isYielding) {
			fiber.itis_syncRes = res;
			return;
		}
		fiber.run(res);
	}
}
function yld() {
	var fiber = Fiber.current;
	var syncRes = fiber.itis_syncRes;
	if (syncRes) {
		fiber.itis_syncRes = null;
		return syncRes;
	}
	fiber.itis_isYielding = true;
	var res = Fiber.yield();
	fiber.itis_isYielding = false;
	return res;
}

function log(msg) {
	console.log(new Date().toJSON() + " " + msg);
}
function jsn(obj) {
	return JSON.stringify(obj);
}
function end() {
	process.exit(0);
}
function chk(condition, msg) {
	if (condition) {
		return;
	}
	var detail = '';
	for (var pos = 2; pos < arguments.length; pos++) {
		var arg = arguments[pos];
		if (arg instanceof Object) {
			detail += ' ' + jsn(arg);
			continue;
		}
		detail += ' ' + arg;
	}
	log('fail at ' + msg + detail);
	process.exit(1);
}

fib(function() {
	log('hello');
	var dbm = require('../lib/dbm.js')();
	var connOut = yld(dbm.connect(cbk('err')));
	if (connOut.err) {
		log('Failed to connect ' + err);
		return;
	}
	log('Connected');
	var poi = require('../lib/poi.js')(dbm);
	var poimodel = poi.poimodel;
	var input = fs.readFileSync('../resources/CityCoords.txt', 'utf8');
	var lines = input.split('\n');
	var guides = [];
	for (var row in lines) {
		var items = lines[row].split('\t');
		var long;
		var lat;
		if (items.length < 3
			|| isNaN(long = parseFloat(items[1]))
			|| isNaN(lat = parseFloat(items[2]))) {
			continue;
		}
		var poi = {
			_id: items[0],
			dispname : items[0],
			description: items[0],
			hasLoc: true,
			location: {
				type : 'Point',
				coordinates: [ long, lat ]
			}
		};
		log(jsn(poi));
		var out = yld(new poimodel(poi).save(cbk('err')));
		log(jsn(out));
	}
	end();
});
