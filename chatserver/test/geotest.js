var geo = require("../geo.js");
var fs = require("fs");
var geotest = (function() {
	var module = {};

	// ### start
	module.start = function() {
		log("geo test");
		mongoose = require('mongoose');
		log("mongoose.connect");
		mongoose.connect('mongodb://127.0.0.1/itisdb', function (error) {
			must(!error, "mongoose.connect error", error);
			log("mongoose.connect okay");
			dispatch();
		});
	};
	var args = {};
	function dispatch() {
		for (var pos = 2; pos + 1 < process.argv.length; pos++) {
			args[process.argv[pos]] = process.argv[pos + 1];
		}
		log("args=" + jsn(args));
		var cmd = args["-cmd"] || args["c"];
		must(cmd, "command -cmd [simple|init|fake|search|clear]");
		if (cmd == "simple" || cmd == "s") {
			return simpleBegin();
		}
		if (cmd == "init" || cmd == "i") {
			return initBegin();
		}
		if (cmd == "fake" || cmd == "f") {
			return fakeBegin();
		}
		if (cmd == "fakeUrl" || cmd == "fu") {
			return fakeUrlBegin();
		}
		if (cmd == "search" || cmd == "se") {
			return searchBegin();
		}
		if (cmd == "clear" || cmd == "c") {
			return clearBegin();
		}
		if (cmd == "random" || cmd == "r") {
			return randomBegin();
		}
		must(false, "unkown cmd", cmd);
	}

	// ### simple
	var studentSchema;
	var Student;
	function simpleBegin() {
		log("### simple");
		var studentSchemaDef = {
			id: String,
			name : {
				firstName : String,
				lastName : String
			},
			age: Number,
			exams : [{
				course: String,
				date: Date,
				score: Number
			}]
		};
		var studentSchema = mongoose.Schema(studentSchemaDef);
		Student = mongoose.model("student", studentSchema);
		Student.remove({ id : /1/ }, function(error) {
			must(!error, "Student.remove", error);
			simpleUpdate();
		});
	}
	var student1;
	var student1dup;
	function simpleUpdate() {
		student1 = new Student({ id : "1" });
		log("student1.save first time");
		var remain = 3;
		student1.save(function(error, row) {
			must(!error, "student1.save first time", error, row);
			if (--remain != 0) {
				return;
			}
			simpleRead();
		});
		log("student1.save second time");
		student1.save(function(error, row) {
			must(!error, "student1.save second time", error, row);
			if (--remain != 0) {
				return;
			}
			simpleRead();
		});
		student1dup = new Student({ id : "1" });
		log("student1dup.save");
		student1dup.save(function(error, row) {
			must(!error, "student1dup.save a duplicate", error, row);
			if (--remain != 0) {
				return;
			}
			simpleRead();
		});
	}
	function simpleRead() {
		Student.find({ id : "1" }, function(error, rows) {
			must(!error, "Student.find", error);
			simpleDump(rows);
		});
	}
	function simpleDump(students) {
		students.forEach(function(student) {
			log("student=" + jsn(student));
		});
		log("student1    = " + jsn(student1));
		log("student1dup = " + jsn(student1dup));
		guideBegin();
	}
	var Guide;
	var guideBeijing;
	var guideHongKong;
	function guideBegin() {
		log("### guide");
		var guideSchema = mongoose.Schema({
			id : String,
			location : {
				type: {
					type: String,
					default: "Point"
				},
				coordinates: [Number]
			}
		});
		guideSchema.index({ location: "2dsphere" });
		guideSchema.pre("save", function (next) {
			//log("presave: " + this);
			if (!this.location) {
				//log("guideSchema.pre save: found a null or undefined location " + this);
				return next(new Error("null location"));
			}
			if (!this.location.coordinates) {
				//log("guideSchema.pre save: found a null or undefined location.coordinate " + this);
				return next(new Error("null coordinate"));
			}
			if (this.location.coordinates.length != 2) {
				//log("guideSchema.pre save: found a non-two-value location.coordinate " + this);
				return next(new Error("non-two-value location.coordinate"));
			}
			return next();
		});
		Guide = mongoose.model("guide", guideSchema);
		Guide.remove({ id : /^Guide in / }, function(error) {
			must(!error, "Guide.remove", error);
			guideUpdate();
		});
	}
	function guideUpdate() {
		var remain = 3 * 2;
		for (var replicate = 0; replicate < 3; replicate++) {
			guideBeijing = new Guide({ id: "Guide in Beijing", location : { coordinates : [116.4, 39.93] } });
			guideBeijing.save(function(error, row) {
				must(!error, "guideBeijing.save", error, row);
				if (--remain != 0) {
					return;
				}
				guideRead();
			});
			guideHongKong = new Guide({ id: "Guide in Hong Kong", location : { coordinates : [114.1747, 22.2783] } });
			guideHongKong.save(function(error, row) {
				must(!error, "guideHongKong.save", error, row);
				if (--remain != 0) {
					return;
				}
				guideRead();
			});
		}
		var guideUnknown = new Guide({ id: "Guide in unknown place" });
		guideUnknown.location = undefined;
		guideUnknown.save(function(error, row) {
			must(error, "guideUnknown.save", error, row);
			log("guideUnknown.save error " + error + " row=" + jsnl(guideUnknown));
		});
		var guideNull = new Guide({ id: "Guide in null place", location : {} });
		guideNull.location.coordinates = undefined;
		guideNull.save(function(error, row) {
			must(error, "guideNull.save", error, row);
			log("guideNull.save error " + error + " row=" + jsnl(guideNull));
		});
		var guideWrong = new Guide({ id: "Guide in wrong location", location : { coordinates : [123] }});
		guideWrong.save(function(error, row) {
			must(error, "guideWrong.save", error, row);
			log("guideWrong.save error " + error + " row=" + jsnl(guideWrong));
		});
		var guideMore = new Guide({ id: "Guide in extra value", location : { coordinates : [123, 45, 67] }});
		guideMore.save(function(error, row) {
			must(error, "guideMore.save", error, row);
			log("guideMore.save error " + error + " row=" + jsnl(guideMore));
		});
	}
	function guideRead() {
		// Distance between Bejing and Hong Kong is about 1,977 kilometers
		var maxDistanceMeter = 1 * 1000 * 1000;
		Guide.find({
			location : {
				$near : {
					$geometry : {
						type : "Point",
						coordinates : [116.4, 39.93]
					},
					$maxDistance : maxDistanceMeter
				}
			}
		})
		.limit(5)
		.exec(function (error, rows) {
			must(!error, "Guide.find", error);
			guideDump(rows);
		});
	}
	function guideDump(guides) {
		log("Guides within 1,000 kilometers of Beijing:");
		guides.forEach(function(guide) {
			log("guide=" + jsn(guide));
		});
		log("guideBeijing  = " + jsn(guideBeijing));
		log("guideHongKong = " + jsn(guideHongKong));
		guideReadFar();
	}
	function guideReadFar() {
		// Distance between Bejing and Hong Kong is about 1,977 kilometers
		var maxDistanceMeter = 2 * 1000 * 1000;
		Guide.find({
			location : {
				$near : {
					$geometry : {
						type : "Point",
						coordinates : [116.4, 39.93]
					},
					$maxDistance : maxDistanceMeter
				}
			}
		})
		.limit(5)
		.exec(function (error, rows) {
			must(!error, "Guide.find", error);
			guideDumpFar(rows);
		});
	}
	function guideDumpFar(guides) {
		log("Guides within 2,000 kilometers of Beijing:");
		guides.forEach(function(guide) {
			log("guide=" + jsn(guide));
		});
		log("guideBeijing  = " + jsn(guideBeijing));
		log("guideHongKong = " + jsn(guideHongKong));
		guideReadAdv();
	}
	function guideReadAdv() {
		// Distance between Bejing and Hong Kong is about 1,977 kilometers
		var maxDistanceMeter = 2 * 1000 * 1000;
		Guide.geoNear({
				type : "Point",
				coordinates : [116.4, 39.93]
			}, {
				spherical : true,
				maxDistance : 2 * 1000 * 1000,
				num: 5,
				query : { id : /^Guide in/ }
			}, function(error, results, stats) {
				must(!error, "Guide.geoNear", error);
				guideDumpAdv(results, stats);
			});
	}
	function guideDumpAdv(results, stats) {
		log("Guides within 2,000 kilometers of Beijing:");
		results.forEach(function(result) {
			log("result.dis=" + result.dis);
			log("result.obj=" + jsnl(result.obj));
		});
		log("stats=" + jsn(stats));
		log("guideBeijing  = " + jsn(guideBeijing));
		log("guideHongKong = " + jsn(guideHongKong));
		log("Spherical distance betwen Beijing and Hong Kong (meter): " + sphericalDistanceMeter(
			guideBeijing.location.coordinates[0],
			guideBeijing.location.coordinates[1],
			guideHongKong.location.coordinates[0],
			guideHongKong.location.coordinates[1]));
		log(sphericalDistanceMeter(0, 0, 90, 0));
		log(sphericalDistanceMeter(0, 0, 0, 90));
		quit();
	}
	var EarthRadiusMeter = 6378160;
	var DegreeToRadian = Math.PI / 180;
	// https://en.wikipedia.org/wiki/Great-circle_distance
	function sphericalDistanceMeter(lon1, lat1, lon2, lat2) {
		var l1 = lon1 * DegreeToRadian;
		var p1 = lat1 * DegreeToRadian;
		var l2 = lon2 * DegreeToRadian;
		var p2 = lat2 * DegreeToRadian;
		return EarthRadiusMeter * Math.acos(
			Math.sin(p1) * Math.sin(p2) + Math.cos(p1) * Math.cos(p2) * Math.cos(l2 - l1));
	}
	function sphericalDistanceMeterHaversine(lon1, lat1, lon2, lat2) {
		var l1 = lon1 * DegreeToRadian;
		var p1 = lat1 * DegreeToRadian;
		var l2 = lon2 * DegreeToRadian;
		var p2 = lat2 * DegreeToRadian;
		var dl = l2 - l1;
		var dp = p2 - p1;
		var sinDp2 = Math.sin(dp / 2);
		var sinDl2 = Math.sin(dl / 2);
		var a = sinDp2 * sinDp2 + Math.cos(p1) * Math.cos(p2) * sinDl2 * sinDl2;
		var ds = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); // = 2 * Math.asin(Math.sqrt(a))
		return EarthRadiusMeter * ds;
	}

	// ### init
	function initBegin() {
		geo.init(mongoose);
		quit();
	}

	// ### fake
	function fakeBegin() {
		geo.init(mongoose);
		var input = fs.readFileSync("./resources/CityCoords.txt", "utf8");
		var lines = input.split("\n");
		var guides = [];
		for (var row in lines) {
			var items = lines[row].split("\t");
			var long;
			var lat;
			if (items.length < 3
				|| isNaN(long = parseFloat(items[1]))
				|| isNaN(lat = parseFloat(items[2]))) {
				continue;
			}
			guides.push({ id : "Foo in " + items[0], long : long, lat : lat });
		}
		var remain = guides.length;
		for (var pos in guides) {
			var guide = guides[pos];
			geo.update(guide.id, guide.long, guide.lat, { guide: true }, function(err) {
				must(!err, "geo.update", err);
				if (--remain == 0) {
					fakeEnd();
				}
			});
		}
	}
	function fakeEnd() {
		quit();
	}

	// ### fakeUrl
	function fakeUrlBegin() {
		var http = require("http");
		var input = fs.readFileSync("./resources/CityCoords.txt", "utf8");
		var lines = input.split("\n");
		var guides = [];
		for (var row in lines) {
			var items = lines[row].split("\t");
			var long;
			var lat;
			if (items.length < 3
				|| isNaN(long = parseFloat(items[1]))
				|| isNaN(lat = parseFloat(items[2]))) {
				continue;
			}
			guides.push({ id : "Foo in " + items[0], long : long, lat : lat });
		}
		var remain = guides.length;
		for (var pos in guides) {
			var guide = guides[pos];
			var path = "/report?usr=" + guide.id.replace(/ /g, "%20") + "&loc=1&long=" + guide.long + "&lat=" + guide.lat;
			log(path);
			http.request({
				host: "127.0.0.1",
				port: "3030",
				path: path
			}, function() {
				if (--remain == 0) {
					fakeUrlEnd();
				}
			}).end();
		}
	}
	function fakeUrlEnd() {
		quit();
	}

	// ### search
	function searchBegin() {
		geo.init(mongoose);
		var long = args["-long"] || 116.4;
		var lat = args["-lat"] || 39.93;
		var maxDistanceDegree = args["-degree"] || 20;
		var limit = args["-limit"] || 10;
		geo.search(long, lat, maxDistanceDegree, { guide: true }, limit, function(err,results) {
			must(!err, "geo.search", err);
			searchDump(results);
		});
	}
	function searchDump(results) {
		results.forEach(function(result) {
			log("result.dis=" + result.dis);
			log("result.obj=" + jsnl(result.obj));
		});
		quit();
	}

	// ### clear
	function clearBegin() {
		geo.init(mongoose);
		geo.delete(/^Foo in/, function(err) {
			must(!err, "geo.delete", err);
			clearConfirm();
		});
	}
	function clearConfirm() {
		geo.search(116.4, 39.93, 20, {}, 10, function(err,results) {
			must(!err, "geo.search", err);
			must(results.length == 0, "results should be empty", results.length);
			quit();
		});
	}

	// ### random
	function randomBegin() {
		var list = [];
		var RadianToDegree = 180 / Math.PI; 
		for (var index = 0; index < 100; index++) {
			/*
			var u = Math.random(); // [0, 1)
			var v = Math.random(); // [0, 1)
			var th = 2 * Math.PI * u; // [0, 2pi)
			var ph = Math.acos(2 * v - 1); // [0, pi)
			var lon = th / Math.PI * 180 - 180; // [-180, 180)
			var lat = ph / Math.PI * 180 - 90; // [-90, 90)
			var txt = "#" + index + " u=" + u + " v=" + v + " th=" + th + " ph=" + ph + " lon=" + lon + " lat=" + lat;
			*/
			var lon = Math.random() * 360 - 180;
			var lat = Math.acos(2 * Math.random() - 1) * RadianToDegree - 90;
			var txt = "#" + index + " lon=" + lon + " lat=" + lat;
			list.push({ lat: lat, txt: txt });
		}
		list.sort(function(a, b) {
			return a.lat - b.lat;
		});
		for (var index in list) {
			log(list[index].lat + " -> " + list[index].txt);
		}
		quit();
	}
	
	// ### helper
	function log(msg) {
		console.log(new Date().toJSON() + " " + msg);
	}
	function jsn(obj) {
		return JSON.stringify(obj);
	}
	function jsnl(obj) {
		return JSON.stringify(obj).replace(/(\r\n|\n|\r)/gm, "");
	}
	function must(condition, msg) {
		if (condition) {
			return;
		}
		var detail = "";
		for (var pos = 2; pos < arguments.length; pos++) {
			var arg = arguments[pos];
			if (arg instanceof Object) {
				detail += " " + jsn(arg);
				continue;
			}
			detail += " " + arg;
		}
		log("fail at " + msg + detail);
		process.exit(1);
	}
	function quit() {
		process.exit(0);
	}
	return module;
})();
geotest.start();
