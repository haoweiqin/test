var Fiber = require('fibers');
main()
function main() {
	log('main');
	var args = {};
	for (var pos = 2; pos + 1 < process.argv.length; pos += 2) {
		args[process.argv[pos]] = process.argv[pos + 1];
	}
	log('args=' + jsn(args));
	var cmd = args['-cmd'] || args['c'];
	if (cmd == 'bcrypt' || cmd == 'b') {
		bcryptBegin();
	}
	if (cmd == 'https' || cmd == 'h') {
		httpsBegin();
	}
	if (cmd == 'httpsApp' || cmd == 'ha') {
		httpsAppBegin(args);
	}
	if (cmd == 'session' || cmd == 's') {
		sessionBegin();
	}
	if (cmd == 'fiber' || cmd == 'f') {
		fiberBegin();
	}
	if (cmd == 'cluster' || cmd == 'c') {
		clusterBegin(args);
	}
	if (cmd == 'ejs' || cmd == 'e') {
		ejsBegin();
	}
	if (cmd == 'canvas' || cmd == 'ca') {
		canvasBegin(args);
	}
	if (cmd == 'register' || cmd == 'r') {
		registerBegin(args);
	}
	if (cmd == 'dev' || cmd == 'd') {
		devBegin(args);
	}
	if (cmd == 'prod' || cmd == 'p') {
		prodBegin(args);
	}
}

function log(msg) {
	console.log(new Date().toJSON() + ' ' + msg);
}
function end() {
	process.exit(0);
}
function chk(condition, msg) {
	if (condition) {
		return;
	}
	log('fail at ' + msg + ' ' + jsn([].splice.call(arguments, 2)));
	process.exit(1);
}
function jsn(obj) {
	return JSON.stringify(obj);
}
function jso(obj) {
	var out = '{';
	var count = 0;
	for (var key in obj) {
		count++;
		out += '"' + key + '":' + jsn(obj[key]) + ',';
	}
	out += '"count":' + count + '}';
	return out;
}

function fib() {
	if (arguments.length == 2) {
		new Fiber(arguments[1]).run(arguments[0]);
		return;
	}
	new Fiber(arguments[0]).run();
}
function cbk() {
	var fiber = Fiber.current;
	var names = [].splice.call(arguments, 0);
	return function() {
		var res = {};
		for (var pos = 0; pos < names.length; pos++) {
			res[names[pos]] = arguments[pos];
		}
		if (!fiber.itis_isYielding) {
			fiber.itis_syncRes = res;
			return;
		}
		fiber.run(res);
	}
}
function yld() {
	var fiber = Fiber.current;
	var syncRes = fiber.itis_syncRes;
	if (syncRes) {
		fiber.itis_syncRes = null;
		return syncRes;
	}
	fiber.itis_isYielding = true;
	var res = Fiber.yield();
	fiber.itis_isYielding = false;
	return res;
}

// sudo npm install bcrypt
function bcryptBegin() {
	log('bcrypt');
	var bcrypt = require('bcrypt');
	var passwordHash;
	bcrypt.hash('bacon', 10, function(err, hash) {
		chk(!err, 'bcrypt.hash', err, hash);
		log('bacon 10 -> ' + hash);
		passwordHash = hash;
		bcryptMatch();
	});
	function bcryptMatch() {
		bcrypt.compare('bacon', passwordHash, function(err, res) {
			chk(!err, 'bcryptMatch', err, res);
			log('bacon match res=' + res);
			bcryptMismatch();
		});
	}
	function bcryptMismatch() {
		bcrypt.compare('wrong', passwordHash, function(err, res) {
			chk(!err, 'bcryptMatch', err, res);
			log('wrong match res=' + res);
			end();
		});
	}
}

function httpsBegin() {
	var https = require('https');
	var fs = require('fs');
	var hskey = fs.readFileSync('localhost-key.pem');
	var hscert = fs.readFileSync('localhost-cert.pem')
	var options = {
	key: hskey,
	cert: hscert
	};
	https.createServer(options, function (req, res) {
		res.writeHead(200);
		res.end('Hi from HTTPS');
	}).listen(8000);
	setTimeout(httpsTest, 1000);

	function httpsTest() {
		var sys = require('sys');
		var exec = require('child_process').exec;
		var cmd = 'curl -k https://localhost:8000/';
		log('exec cmd: ' + cmd);
 		exec(cmd, httpsDump);
	}
	function httpsDump(error, stdout, stderr) {
		log('error: ' + error);
		log('stdout: ' + stdout);
		log('stderr: ' + stderr);
		chk(!error, 'https fail', error);
	}
}

/*
Limitation
	There are two kinds of certificates: those signed by a 'Certificate Authority', or CA, and 'self-signed certificates'
	 In most cases, you would want to use a CA-signed certificate in a production environment
	 For testing purposes, however, a self-signed certicate will do just fine.
Generation of the Self-Signed Certificate
	Refer to http://www.hacksparrow.com/node-js-https-ssl-certificate.html
		openssl genrsa -out localhost-key.pem 1024
		openssl req -new -key localhost-key.pem -out certrequest.csr
			Country Name (2 letter code) [AU]:US
			State or Province Name (full name) [Some-State]:Washington
			Locality Name (eg, city) []:Redmond
			Organization Name (eg, company) [Internet Widgits Pty Ltd]:ITIS
			Organizational Unit Name (eg, section) []:R&D
			Common Name (e.g. server FQDN or YOUR name) []:localhost
				NOTE: Common Name must match the host-name in the URL: https://<host-name>:<port>/
			Email Address []:itravelishow@outlook.com
			Please enter the following 'extra' attributes
			to be sent with your certificate request
			A challenge password []:<hidden>
			An optional company name []:itis
		openssl x509 -req -in certrequest.csr -signkey localhost-key.pem -out localhost-cert.pem
Setup in Server
	https://aws.amazon.com/console/
		Login
	Instance - woxingwoxiu, security group = itisadmin
	Security - itisadmin - Inbound - Edit
		Custom TCP Rule | TCP | 8080 | 0.0.0.0/0
	Either
		node ./sectest.js -cmd ha
	Or
		node ./sectest.js -cmd ha -host 52.26.166.250 -loopback localhost -port 8080
	Test (ignore browser warnings)
		https://52.26.166.250:8080
		https://ec2-52-26-166-250.us-west-2.compute.amazonaws.com:8080/
		curl -k https://52.26.166.250:8080
		curl -k https://ec2-52-26-166-250.us-west-2.compute.amazonaws.com:8080/
*/
function httpsAppBegin(args) {
	var host = args['-host'] || 'localhost';
	var loopback = args['-loopback'] || host;
	var port = parseInt(args['-port'] || '8080');
	var sys = require('sys');
	var exec = require('child_process').exec;
	httpsAppStopPrev();

	function httpsAppStopPrev() {
		var cmd = 'curl -k https://' + loopback + ':' + port + '/stop';
		log('exec cmd: ' + cmd);
 		exec(cmd, httpsAppStopPrevDone);
	}
	function httpsAppStopPrevDone(error, stdout, stderr) {
		log('error: ' + error);
		log('stdout: ' + stdout);
		log('stderr: ' + stderr);
		var hasPrev = !error;
		if (!hasPrev) {
			return httpsAppSetup();
		}
		setTimeout(httpsAppSetup, 2000);
	}
	function httpsAppSetup() {
		var https = require('https');
		var fs = require('fs');
		var options = {
		key: fs.readFileSync(host + '-key.pem'),
		cert: fs.readFileSync(host + '-cert.pem')
		};
		var express = require('express');
		var app = express();
		app.get('/', function(req, res) {
			log('app.get /');
			res.send('<h1>Hello world via HTTPS</h1>');
		});
		app.get('/stop', function(req, res) {
			log('app.get /stop');
			res.send('<h1>Stopping HTTPS Server</h1>');
			setTimeout(function () {
				end();
			}, 500);
		});
		https.createServer(options, app).listen(port, function(){
			log('listening on https://' + host + ':' + port);
		});
		setTimeout(httpsAppTest, 1000);
	}
	function httpsAppTest() {
		var cmd = 'curl -k https://' + loopback + ':' + port + '/';
		log('exec cmd: ' + cmd);
 		exec(cmd, httpsAppDump);
	}
	function httpsAppDump(error, stdout, stderr) {
		log('error: ' + error);
		log('stdout: ' + stdout);
		log('stderr: ' + stderr);
		chk(!error, 'https fail', error);
	}
}

// sudo npm install cookie-parser
// sudo npm install express-session
function sessionBegin() {
	var exec = require('child_process').exec;
	sessionDedupe();
	function sessionDedupe() {
		var cmd = 'curl http://localhost:8080/stop';
		log('exec cmd: ' + cmd);
 		exec(cmd, function(lone) {
 			log('dedupe: ' + jsn([].splice.call(arguments, 2)));
 			return lone ? sessionSetup() : setTimeout(sessionSetup, 1000);
 		});
	}
	function sessionSetup() {
		var express = require('express');
		var app = express();
		var cookieParser = require('cookie-parser');
		app.use(cookieParser());
		var session = require('express-session');
		app.use(session({
			secret: Math.random().toString(),
			resave: false,
			saveUninitialized: false
		}));
		var links = '<br/><a href=/awesome>awesome</a><br/><a href=/radical>radical</a><br/><a href=/tubular>tubular</a>';
		app.get('/awesome', function(req, res) {
			log('app.get awesone session=' + jsn(req.session));
			res.contentType('text/html');
			if(req.session.lastPage) {
				res.write('Last page was: ' + req.session.lastPage + '. ');
			}
			req.session.lastPage = '/awesome';
			res.write('Your Awesome.' + links);
			res.end();
		});
		app.get('/radical', function(req, res) {
			log('app.get awesone session=' + jsn(req.session));
			res.contentType('text/html');
			if(req.session.lastPage) {
				res.write('Last page was: ' + req.session.lastPage + '. ');
			}
			req.session.lastPage = '/radical';
			res.write('What a radical visit!' + links);
			res.end();
		});
		app.get('/tubular', function(req, res) {
			log('app.get awesone session=' + jsn(req.session));
			res.contentType('text/html');
			if(req.session.lastPage) {
				res.write('Last page was: ' + req.session.lastPage + '. ');
			}
			req.session.lastPage = '/tubular';
			res.write('Are you a surfer?' + links);
			res.end();
		});
		app.get('/stop', function(req, res) {
			res.contentType('text/html');
			log('app.get /stop');
			res.write('<h1>Stopping HTTP Server</h1>');
			setTimeout(function () { end(); }, 500);
		});
		app.listen(8080);
	}
}

// sudo npm install fibers
function fiberBegin() {
	Fiber(slowTable).run();
	function slowTable() {
		var fiber = Fiber.current;
		for (var i = 2; i <= 9; i++) {
			for (var j = 2; j <= 9; j++) {
				setTimeout(function() {
					fiber.run(i * j);
				}, 100);
				var product = Fiber.yield();
				log(i + '*' + j + '=' + product);
			}
		}
	}
}

function clusterBegin(args) {
	var cluster = require('cluster');
	var http = require('http');
	var numCpu = require('os').cpus().length;
	var numWorker = Math.max(numCpu, 4);
	if (cluster.isMaster) {
		var id = 'master: ';
		log(id + 'master enter init');
		// Fork workers.
		for (var i = 0; i < numWorker; i++) {
			cluster.fork();
		}
		cluster.on('exit', function(worker, code, signal) {
			log('worker ' + worker.process.pid + ' died');
		});
		log(id + 'master leave init');
		fib(args, pingWorkers);
		function pingWorkers(args) {
			var exec = require('child_process').exec;
			var cmd = 'curl -s http://localhost:8000/';
			var remain = numWorker * 2;
			while (remain > 0) {
				var cmdRes = yld(exec(cmd, cbk('err', 'stdout', 'stderr')));
		 		if (cmdRes.err) {
		 			log('curl fail as ' + cmdRes.err);
			 		log(id + cmdRes.stderr);
			 		log(id + cmdRes.stdout);
		 			continue;
		 		}
		 		remain--;
		 	}
		 	if (!args['-once']) {
		 		return;
		 	}
	 		end();
		}
	} else {
		var id = 'worker#' + cluster.worker.id + ': ';
		log(id + 'worker enter init');
		// Workers can share any TCP connection
		// In this case its a HTTP server
		http.createServer(function(req, res) {
			log(id + 'get /');
			res.writeHead(200);
			res.end('response from worker # ' + cluster.worker.id);
		}).listen(8000);
		log(id + 'worker leave init');
	}
}

// sudo npm install ejs
//http://52.26.166.250:8080/ajax/captcha
function ejsBegin() {
	fib(ejsTest);
	function ejsTest() {
		log('ejs');
		log('dedupe');
		var exec = require('child_process').exec;
		var cmd = 'curl http://localhost:8080/stop';
		log(cmd);
		var cmdRes = yld(exec(cmd, cbk('err', 'stdout', 'stderr')));
		log('cmdRes=' + jsn(cmdRes));
 		if (!cmdRes.err) {
			log('wait the duplicate to quit');
 			yld(setTimeout(cbk(), 1000));
 		}
		var express = require('express');
		var app = express();
		require('ejs');
		app.set('view engine', 'ejs');
		app.get('/', function(req, res) {
			log('app.get /');
			res.render('pages/index', { query : req.query });
		});
		app.get('/about', function(req, res) {
			log('app.get /about');
			res.render('pages/about');
		});
		app.get('/stop', function(req, res) {
			fib(function() {
				log('app.get /stop');
				res.send('<h1>Stopping HTTP Server</h1>');
				yld(setTimeout(cbk(), 500));
				log('end');
				end();
			});
		});
		app.get('/ajax/hello', function(req, res) {
			log('app.get /ajax/hello');
			res.send(jsn({query : req.query }));
		});
		var Canvas = require('canvas');
		app.get('/ajax/captcha', function(req, res) {
			fib(function() {
				var canvas = new Canvas(200, 150);
				var ctx = canvas.getContext('2d');
				ctx.fillStyle = '#def';
				ctx.fillRect(0, 0, 200, 150);
				ctx.fillStyle = '#abc';
				ctx.fillRect(15, 15, 120, 120);
				ctx.save();
				var buffer = yld(canvas.toBuffer(cbk('err', 'buf')));
				chk(!buffer.err, 'canvas.toBuffer', buffer.err);
				res.type('jpg');
				res.end(buffer.buf);
			});
		});
		app.listen(8080);
		log('listening, try http://localhost:8080/?abc=123');
		for(;;) {
			var pingRes = yld(exec('curl http://localhost:8080/?abc=123', cbk('err')));
			if (!pingRes.err) {
				break;
			}
		}
		log('passed self-test');
	}
}

// canvas:
//   OSX
// https://github.com/Automattic/node-canvas/wiki/Installation---OSX
// https://github.com/Automattic/node-canvas/issues/348
// https://gist.github.com/darul75/9125097
//   https://github.com/Automattic/node-canvas/wiki/Installation---Ubuntu-and-other-Debian-based-systems
// sudo npm install canvas
//	https://github.com/Automattic/node-canvas/wiki/Installation---Ubuntu-and-other-Debian-based-systems
//sudo apt-get update 
//sudo apt-get install libcairo2-dev libjpeg8-dev libpango1.0-dev libgif-dev build-essential g++
function canvasBegin(args) {
	fib(function() {
		var Canvas = require('canvas');
		var canvas = new Canvas(200, 150);
		var ctx = canvas.getContext('2d');
		ctx.fillStyle = '#def';
		ctx.fillRect(0, 0, 200, 150);
		ctx.fillStyle = '#abc';
		ctx.fillRect(15, 15, 120, 120);
		ctx.save();
		var buffer = yld(canvas.toBuffer(cbk('err', 'buf')));
		chk(!buffer.err, 'canvas.toBuffer', buffer.err);
		log(jsn(buffer));
		var fs = require('fs');
		fs.writeFileSync(args['-out'] || './a.jpg', buffer.buf);
	});
}

// node ./sectest.js -cmd register -senderPasswordFile /Users/Lilian/BitBucket/sec/a.txt
// https://localhost:8080/signup
function registerBegin(args) {
	fib(function() {
		var host = args['-host'] || 'localhost';
		var port = parseInt(args['-port'] || '8080');

		var sys = require('sys');
		var exec = require('child_process').exec;
		dedupe();
		function dedupe()
		{
			var cmd = 'curl -k https://localhost:' + port + '/stop';
			log('exec cmd: ' + cmd);
			var res = yld(exec(cmd, cbk('err', 'stdout', 'stderr')));
			log('res=' + jsn(res));
			if (args['-stop'] == '1') {
				end();
			}
			if (res.err) {
				return;
			}
			yld(setTimeout(cbk(), 2000));
		}

		var https = require('https');
		var fs = require('fs');
		var options = {
			key: fs.readFileSync(host + '-key.pem'),
			cert: fs.readFileSync(host + '-cert.pem')
		};
		var express = require('express');
		var app = express();
		require('ejs');
		app.set('view engine', 'ejs');
		app.get('/', function(req, res) {
			log('app.get /');
			res.send('<h1>Hello world via HTTPS</h1><a href="/signup">Sign up</a>');
		});
		app.get('/stop', function(req, res) {
			fib(function() {
				log('app.get /stop');
				res.send('<h1>Stopping HTTPS Server</h1>');
				yld(setTimeout(cbk(), 500));
				end();
			});
		});

		app.get('/signup', function(req, res) {
			log('app.get /signup');
			res.render('pages/signup');
		});

		var mongoose = require('mongoose');
		dbConnect();
		function dbConnect() {
			log('mongoose.connect');
			var connectRes = yld(mongoose.connect('mongodb://127.0.0.1/itisdb', cbk('err')));
			chk(!connectRes.err, 'mongoose.connect failed', connectRes.err);
		}

		var Student;
		modelInit();
		function modelInit() {
			var studentSchema = mongoose.Schema({
				dispname: String,
				passwordEncrypted: String,
				emailVerified: String
			});
			Student = mongoose.model('student', studentSchema);
			log('Student=' + jsn(Student));
		}

		var signup = require('../lib/signup');
		signupInit();
		function signupInit() {
			var senderPasswordFile = args['-senderPasswordFile'] || args['spf'];
			chk(senderPasswordFile, 'missing -senderPasswordFile parameter');
			var senderPassword = fs.readFileSync(senderPasswordFile, 'utf8');
			var noSend = '1' == (args['-noSend'] || args['ns']);
			signup.init(Student, senderPassword, noSend);
		}

		app.get('/ajax/nameChk', function(req, res) {
			fib(function() {
				var name = dec(req.query.name);
				log('app.get /ajax/nameChk name=' + name);
				var out = yld(signup.nameChk(name, cbk('err')));
				return res.send(jsn(out));
			});
		});

		app.get('/ajax/passChk', function(req, res) {
			var pass = dec(req.query.pass);
			log('app.get /ajax/passChk pass=' + pass);
			var out = signup.passChk(pass);
			return res.send(jsn(out));
		});
		
		var cookieParser = require('cookie-parser');
		app.use(cookieParser());
		var session = require('express-session');
		app.use(session({
			secret: Math.random().toString(),
			resave: false,
			saveUninitialized: false
		}));

		app.get('/captcha.jpg', function(req, res) {
			fib(function() {
				log('app.get /captch.jpg session=' + jsn(req.session));
				var out = yld(signup.captchaImg(req.session, cbk('err', 'img')));
				if (out.err) {
					return res.status(404).send(out);
				}
				res.type('jpg');
				res.end(out.img);
			});
		});

		app.get('/ajax/captchaRegen', function(req, res) {
			log('app.get /ajax/captchaRegen session=' + jsn(req.session));
			signup.captchaRegen(req.session);
			res.send(jsn({}));
		});

		app.get('/ajax/captchaChk', function(req, res) {
			var input = dec(req.query.captcha);
			log('app.get /ajax/captchaChk input=' + input);
			var out = signup.captchaChk(req.session, input);
			res.send(jsn(out));
		});

		app.get('/ajax/emailSend', function(req, res) {
			fib(function () {
				var email = dec(req.query.email);
				log('app.get /ajax/emailSend email=' + email);
				var out = yld(signup.emailSend(req.session, email, cbk('err')));
				res.send(jsn(out));
			});
		});

		app.get('/ajax/emailCodeChk', function(req, res) {
			var emailCode = dec(req.query.emailCode);
			log('app.get /ajax/emailCodeChk emailCode=' + emailCode);
			var out = signup.emailCodeChk(req.session, emailCode);
			res.send(jsn(out));
		});

		app.get('/ajax/signup', function(req, res) {
			fib(function () {
				var name = dec(req.query.name);
				var pass = dec(req.query.pass);
				var captcha = dec(req.query.captcha);
				var emailCode = dec(req.query.emailCode);
				log('app.get /ajax/signup name=' + name + ' captcha=' + captcha + ' emailCode=' + emailCode);
				var out = yld(signup.signup(req.session, name, pass, captcha, emailCode, cbk('err', 'loginUser')));
				if (out.err) {
					return res.send(jsn(out));
				}
				req.session.loginUser = out.loginUser;
				res.send(jsn(out));
			});
		});

		app.get('/welcome', function(req, res) {
			log('app.get /welcome session=' + req.session);
			var loginUser = req.session.loginUser;
			res.send(loginUser
				? '<h1>Welcome! You are successfully logged in as ' + loginUser.dispname + ' </h1><br/><a href=/signout>Sign Out</a><br/><a href=/signup>Sign Up New User</a>'
				: '<h1>You are not logged in</h1><br/><a href=/signin>Sign In</a><br/><a href=/signup>Sign Up New User</a>');
		});

		app.get('/signin', function(req, res) {
			log('app.get /signin');
			res.render('pages/signin');
		});

		app.get('/signout', function(req, res) {
			log('app.get /signout session=' + req.session);
			req.session.loginUser = null;
			res.send('<h1>You are logged out</h1><br/><a href=/signin>Sign In Again</a><br/><a href=/signup>Sign Up New User</a>');
		});

		app.get('/ajax/signin', function(req, res) {
			fib(function () {
				var name = dec(req.query.name);
				var pass = dec(req.query.pass);
				log('app.get /ajax/signin name=' + name);
				var out = yld(signup.signin(name, pass, cbk('err', 'loginUser')));
				if (out.err) {
					return res.send(jsn(out));
				}
				req.session.loginUser = out.loginUser;
				res.send(jsn(out));
			});
		});

		https.createServer(options, app).listen(port, function(){
			log('listening on https://*:' + port);
		});
		yld(setTimeout(cbk(), 1000));
		selfTest();
		function selfTest() {
			var cmd = 'curl -k https://localhost:' + port + '/';
			log('exec cmd: ' + cmd);
			var res = yld(exec(cmd, cbk('err', 'stdout', 'stderr')));
			log('res=' + jsn(res));
			chk(!res.err, 'self test failed', res.err);
			log('self test okay');
		}

		function dec(encoded) {
			return encoded ? decodeURIComponent(escape(fromHex(encoded))) : null;
		}
		function fromHex(encoded) {
			var out = '';
			for (var pos = 0; pos + 1 < encoded.length; pos += 2) {
				out += String.fromCharCode(parseInt(encoded.substring(pos, pos + 2), 16));
			}
			return out;
		}
	});
}

function devBegin() {
	fib(function(){
		sessionDedupe();
		function sessionDedupe() {
			var exec = require('child_process').exec;
			var cmd = 'curl http://localhost:8080/stop';
			log('exec cmd: ' + cmd);
	 		var res = yld(exec(cmd, cbk('err')));
	 		if (res.err) {
	 			return;
	 		}
	 		yld(setTimeout(cbk(), 1000));
		}
		var fs = require('fs');
		hideFolder(__dirname + '/../node_modules/canvas');
		hideFolder(__dirname + '/../node_modules/bcrypt');
		function hideFolder(folder) {
			var statRes = yld(fs.stat(folder, cbk('err', 'stat')));
			if (statRes.err) {
				return log('folder ' + folder + ' not exist');
			}
			log('try rename ' + folder + ' to ' + folder + '_');
			var renameRes = yld(fs.rename(folder, folder + '_', cbk('err')));
			chk(!renameRes.err, 'rename', folder);
		}
		changeConfig();
		function changeConfig() {
			var input = fs.readFileSync(__dirname + '/../config.json', 'utf8');
			var output = input
				.replace('/root/sec/a.txt', '/Users/Lilian/BitBucket/sec/a.txt')
				.replace('"noSendEmail" : false', '"noSendEmail" : true');
			fs.writeFileSync(__dirname + '/../config.json', output);
		}
		require(__dirname + '/../index.js');
		var express = require('express');
		var app = express();
		app.get('/stop', function(req, res) {
			fib(function() {
				log('app.get /stop');
				res.send('<h1>Stopping HTTP Server</h1>');
				yld(setTimeout(cbk(), 500));
				log('end');
				end();
			});
		});
		app.listen(8080);
	});
}

function prodBegin() {
	fib(function(){
		var fs = require('fs');
		showFolder(__dirname + '/../node_modules/canvas');
		showFolder(__dirname + '/../node_modules/bcrypt');
		function showFolder(folder) {
			var statRes = yld(fs.stat(folder + '_', cbk('err', 'stat')));
			if (statRes.err) {
				return log('folder ' + folder + '_ not exist');
			}
			log('try rename ' + folder + '_ to ' + folder);
			var renameRes = yld(fs.rename(folder + '_', folder, cbk('err')));
			chk(!renameRes.err, 'rename', folder);
		}
		changeConfig();
		function changeConfig() {
			var input = fs.readFileSync(__dirname + '/../config.json', 'utf8');
			var output = input
				.replace('/Users/Lilian/BitBucket/sec/a.txt', '/root/sec/a.txt')
				.replace('"noSendEmail" : true', '"noSendEmail" : false');
			fs.writeFileSync(__dirname + '/../config.json', output);
		}
		var sys = require('sys');
		var exec = require('child_process').exec;
		var cmd = 'curl http://localhost:8080/stop';
		log('exec cmd: ' + cmd);
		var cmdRes = yld(exec(cmd, cbk('err', 'stdout', 'stderr')));
		log('cmdRes = ' + jsn(cmdRes));
	});
}
