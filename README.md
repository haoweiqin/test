##########
# IMPORTANT #
Deployment process

### git clone -b dev  https://<UserName>@bitbucket.org/itisadmin/itisserver.git


# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo is storing all server side files, including
  - website node js
  - rest api js
  - chat server js
  - mongobd info

* Version

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Folder structure ###

      [chat server contains all codes that expose the mongodb data to client ]
      -chatserver
       
       [Self-owned Node js modules]
       +lib
         [module that create and update chatgroup in mongodb]
        -chatgrp.js
         [module that listen client socket io request and handle requests]
        -chatsrv.js
         [module that connect to mongodb]
        -dbm.js
         [module that create and update message in mongodb]
        -msg.js
         [module that create/update user in mongodb]
        -user.js

       [Installed 3rd-party node modules will be checked in here]
       +node_modules

       +resources
       
       +test
      
       [utility function put under this folder]
       +util
         [log utility]
        -util.js
       
       [ejs pages]
       +views
        -pages

       [main node module that drives all REST and socket io calls from client]
       [NOTE: using require('./lib/xxx.js') to include the reference to the module xxx]
       +index.js
  
       [Entry page of node js, a chatting room test page]
       +index.html

      [design document]
      -doc

      [tools and pem/ppk files that used during development]
      -tool

      [website nodejs and content for itravelishow.com]
      -website


### How do I checkin a new node module ###
    [1] goto any itisserver enlistment
    [2] cd <enlistroot>/chatserver
    [3] npm install <module>
    [4] verify 'node index.js' works
    [5] check the new node module in using git
    [6] Run 'deploy.sh' latest to publish the new commit


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
  
  -MonboDB was setup on /dev/sdc
  
  -Server (NodeJs) use Mongoose to connect and manipulate the database (see index.js)

  -Client talk to server using socketio and REST API.

  -Server Node to expose REST by talking to mongodb

     [1] Database name: itisdb
   
     [2] Export create/read/update/delete operation of User and GPS location Data
  
     [3] User collection already created within itisdb (database)
  
     [4] User will have home address info and GPS data, if GPS data is not available then fallback to home address
  
  -How to access the EC2 machine and try mongodb
  
    [1] Enlist source code of itiserver

    [2] Unzip tool/windows/kkk.zip, PLEASE ask Haowei for password to unzip
     
    [3] Run ssh to connect to ec2 windows by following command line -make sure itisadmin.pem under current directory
       
       ssh -i "itisadmin.pem" ubuntu@52.26.166.250

    [4] After login, run 'sudo su -' as root if needed
     
    [5] Run 'mongo' to talk to mongodb server
         
         Then 'use itisdb'
         
         Then 'db.user.find()'

    [6] Please try to find mongo command line to add collection, document, database, etc.
    
  -How to install self-signed certificate to Mac machine? So browser shows the certificate from https://<IP>:3030 as valid certificate

    [1] sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain 52.26.166.250.cer 


* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact